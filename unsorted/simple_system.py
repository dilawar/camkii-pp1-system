"""simple_system.py: 

In this we implement only the phosphorylation steps of CaMKII and plot the
kinase activity as function of active CaMKII.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Dilawar Singh"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import numpy as np

import moose
print( moose.__file__ )
import moose.utils as mu

def make_model( c ):
    cpath = c.path
    species = { }
    tables = { }
    for a in [ 'x0', 'x1', 'x*', 'x2', 'pp1', 'cplx', 'pprate' ]:
        t = moose.Table2( '%s/%s_tab' % (cpath, a ) )
        species[ a ] = moose.Pool( '%s/%s' % (cpath, a ) )
        species[ a ].concInit = 0.0
        moose.connect( t, 'requestOut', species[a], 'getConc' )
        tables[ a ] = t

    species[ 'x0' ].concInit = 1

    # Reactions
    v1 = 0.1
    r = moose.Reac( '%s/r1' % cpath )
    moose.connect( r, 'sub', species[ 'x0'] , 'reac' )
    moose.connect( r, 'prd', species[ 'x1'] , 'reac' )
    r.Kf = v1 ** 2
    r.Kb = 0.0

    r = moose.Reac( '%s/r2' % cpath )
    moose.connect( r, 'sub', species[ 'x1'] , 'reac' )
    moose.connect( r, 'sub', species[ 'x1'] , 'reac' )
    moose.connect( r, 'prd', species[ 'x*'] , 'reac' )
    r.Kf = v1
    r.Kb = 0

    r = moose.Reac( '%s/r3' % cpath )
    moose.connect( r, 'sub', species[ 'x*'] , 'reac' )
    moose.connect( r, 'prd', species[ 'x1'] , 'reac' )
    moose.connect( r, 'prd', species[ 'x2'] , 'reac' )
    r.Kf = 1e3 * v1
    r.Kb = 0

    ## # Phosphatase
    ## species[ 'pp1' ].concInit = 0.01

    ## r = moose.Reac( '%s/rpp1' % cpath )
    ## moose.connect( r, 'sub', species[ 'x2'] , 'reac' )
    ## moose.connect( r, 'sub', species[ 'pp1'] , 'reac' )
    ## moose.connect( r, 'prd', species[ 'cplx'] , 'reac' )
    ## r.Kf = 0.5
    ## r.Kb = 0

    ## r = moose.Reac( '%s/rpp2' % cpath )
    ## moose.connect( r, 'sub', species[ 'cplx'] , 'reac' )
    ## moose.connect( r, 'prd', species[ 'pp1'] , 'reac' )
    ## moose.connect( r, 'prd', species[ 'x0'] , 'reac' )
    ## moose.connect( r, 'prd', species[ 'pprate'] , 'reac' )
    ## r.Kf = 10
    ## r.Kb = 0

    # Solver 
    stoich = moose.Stoich( '%s/stoich' % cpath )
    ks = moose.Ksolve( '%s/ksolve' % cpath )
    stoich.ksolve = ks
    stoich.compartment = c 
    stoich.path = '%s/##' % cpath
    
    return tables

def main( ):
    c = moose.CubeMesh( '/compt' )
    c.volume = 1
    tables = make_model( c )
    moose.reinit( )
    moose.start( 1e4 )

    ax = plt.subplot( 2, 1, 1 )

    x2 = tables[ 'x2' ].vector
    pp1 = tables[ 'pprate' ].vector
    x2r = np.diff( x2 )
    ax.plot( x2[1:], x2r )
    ax.set_xlabel( 'Active camkii (x2)' )
    ax.set_ylabel( 'Rates of kinase' )

    ax = plt.subplot( 2, 1, 2 )
    del tables[ 'pprate' ]
    mu.plot_records( tables, ax = ax )

if __name__ == '__main__':
    main()
