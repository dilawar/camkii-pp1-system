"""weakly_coupled_camkii_process_data.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2015, Dilawar Singh and NCBS Bangalore"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import numpy as np
import matplotlib.pyplot as plt

def main( npyfile ):
    tables = np.load( npyfile )
    for t in tables:
        print t
        records[t] = np.sum([ x for x in tables[t] ], axis = 0 )

    stamp = datetime.datetime.now().isoformat()
    dataFile = '_data/%s_%s.csv.gz' % ( 'camkii_pp1_switch', stamp )
    mu.saveRecords( records, outfile = dataFile )
    
    tVec = np.linspace( 0, moose.Clock('/clock').currentTime, len(records['x0y6']))
    tVec = tVec / (24 * 3600 )

    # plt.figure( figsize = (20, 10) )
    plt.subplot( 3, 1, 1 )
    activeCaMCAII = records['x6y0'] + records['x5y1'] + records['x4y2'] + \
            records['x7y0'] + records['x6y1'] + records['x5y2'] 
    plt.plot( tVec, activeCaMCAII, label = 'CaMKII*', alpha = 1 )
    plt.legend(loc='best', framealpha=0.4)
    plt.xlabel( 'Time (days)' )

    plt.subplot(3, 1, 2)
    plt.plot( tVec, records['y'], label = 'y', alpha = 0.3)
    plt.plot( tVec, records['x'], label = 'x (P)')
    plt.xlabel( 'Time (days)' )
    plt.legend( framealpha=0.4)

    plt.subplot( 3, 1, 3)
    plt.hist( activeCaMCAII, bins = int(max(activeCaMCAII)) + 1 )

    plt.suptitle( stamp, fontsize = 10 )
    plt.tight_layout( )
    outfile = '_images/weakly_coupled_%s.png' % stamp
    print('[INFO] Saving result to %s' % outfile )
    plt.savefig( outfile )
    outfile = 'weakly_coupled.png' 
    print('[INFO] Saving result to %s' % outfile )
    plt.savefig( outfile )
    plt.close()

if __name__ == '__main__':
    import sys
    main( sys.argv[1] )
