"""find_transitions.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Dilawar Singh"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas

def transitions( vec, thres ):
    try:
        vec = vec.as_matrix( )
    except Exception as e:
        pass
    data = vec.copy( )
    min, max = data.min(), data.max()
    data[ data <= min + thres ] = min
    data[ data >= max - thres ] = max
    for i, d in enumerate( data[1:] ):
        if data[i] == min and d != max:
           data[i+1] = min 
        elif data[i] == max and d != min:
            data[i+1] = max
    # print data.min(), data.max()
    return data


def main( ):
    # filepath = sys.argv[1]
    # data = pandas.read_csv( filepath )
    size = 1000
    data = np.random.random( size )
    transitions( data, thres = 0.2 )


if __name__ == '__main__':
    main()
