/* CaMKII and PP1 system in maxima */
load( contrib_ode )$
ratprint:false$

/* debugmode: true; */

kh1 : 0.7e-6;
kh2 : 0.3e-6;
k1 : 1.5;
k2 : 10;

/* Calcium input */
/* ca(t) := block( 1e-6*sin(t) ); */

/* CaMKII phosphorylation */
v1(t) := block(k1 * (ca(t)/kh1)^3 / (1+(ca(t)/kh1)^3))^3;
v2(t) := block(k1 * (ca(t)/kh2)^3 / (1+(ca(t)/kh2)^3));

reac0to2 : 'diff(x6y0(t),t) = - v1(t) * x4y2(t);
reac2to6 : 'diff(x4y2(t),t) = - v2(t) * x4y2(t) + v1(t) * x6y0(t);

/* dephosphorylation */
sol : ode2( [ reac0to2, reac2to6 ], [ x6y0 ], t  );
