#!/bin/bash - 

set -o nounset                              # Treat unset variables as an error
set -e
set -x
DATADIR=exp_effect_of_subunit_exchange_on_turnover_sensitivity
mkdir -p $DATADIR
camkii=8
pp1=12
turnover_rate=$(echo "1/30/3600/2" | bc -l)
for (( i = 1; i < 5; i++ )); do
    turnover_rate=$(echo "$turnover_rate * 2" | bc -l)      # Increase twice
    echo "No of PP1 molecules ${pp1}. Running for 300 days, recording every min"

    # Run analyze and store the data.
    python ./camkii_pp1_scheme.py --camkii ${camkii} --pp1 ${pp1} \
        --simtime 300 -dt 60 \
        --turnover-rate "${turnover_rate}" \
        --enable-subunit-exchange
    python ./analyze.py -i ./camkii_pp1_scheme.py.dat \
        --title "CaMKII=${camkii}, PP1=${pp1}"

    # Append SUFFIX and archive
    SUFFIX="_TR_${turnover_rate}_with_subunit_exchange"
    mv ./camkii_pp1_scheme.py.dat \
        $DATADIR/data_camkii${camkii}_pp${pp1}_$SUFFIX.dat
    mv ./camkii_pp1_scheme.py.dat.png $DATADIR/plot_${camkii}_pp${pp1}_$SUFFIX.png

    # Similarly again but without any subunit exchange enabled.
    SUFFIX="_TR_${turnover_rate}"
    pp1=11
    python ./camkii_pp1_scheme.py --camkii ${camkii} --pp1 ${pp1} --simtime 300 -dt 60 
    python ./analyze.py -i ./camkii_pp1_scheme.py.dat \
        --title "CaMKII=${camkii}, PP1=${pp1}"
    mv ./camkii_pp1_scheme.py.dat \
        $DATADIR/data_camkii${camkii}_pp${pp1}_${SUFFIX}.dat
    mv ./camkii_pp1_scheme.py.dat.png \
        $DATADIR/plot_${camkii}_pp${pp1}_${SUFFIX}.png
done
