#!/bin/bash - 

set -o nounset                              # Treat unset variables as an error
set -e
set -x
DATADIR=exp_effect_of_subunit_exchange
mkdir -p $DATADIR
camkii=8
pp1=8
for (( i = 10; i < 18; i++ )); do
    pp1=$i
    SUFFIX=""
    if [[ "$1" == *enable-subunit-exchange* ]]; then
        PYARGS="--enable-subunit-exchange"
    fi
    echo "No of PP1 molecules ${pp1}. Running for 300 days, recording every min"
    python ./camkii_pp1_scheme.py --camkii ${camkii} --pp1 ${pp1} \
        --simtime 300 -dt 60 "${PYARGS}"
    python ./analyze.py -i ./camkii_pp1_scheme.py.dat \
        --title "CaMKII=${camkii}, PP1=${pp1} $@"
    cp ./camkii_pp1_scheme.py.dat $DATADIR/data_camkii${camkii}_pp${pp1}_$SUFFIX.dat
    cp ./camkii_pp1_scheme.py.dat.png $DATADIR/plot_${camkii}_pp${pp1}_$SUFFIX.png
done
