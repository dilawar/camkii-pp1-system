#!/bin/bash
set -e
set -x
#unset PYTHONPATH
echo `python -c "import moose; print moose.__file__"`
mkdir -p _images
MODEL=camkii_pp1_scheme.py
echo "++ Running $MODEL"
##if [ $# -lt 1 ]; then 
##    python $MODEL -h
##    exit
##fi
CAMKII=21
PP1=$(python -c "print(int(${CAMKII}*1.3))")
VOLUME="3e-20"
SIMTIME=`echo "365*24*3600" | bc`
# NOTE: The basal concentration is 80 nano-molar which can go upto 160 nm in
# fluctuations. For these values, I should change my model.
CONC_LOW="0.0000800"
CONC_HIGH="0.000160"
for CAMKII in {15..30}; do
    DATFILE="camkii_vol${VOLUME}_camkii${CAMKII}_pp${PP1}_ca${CONC_HIGH}.dat"
    time python ./camkii_pp1_scheme.py -st $SIMTIME -cl $CONC_LOW -ch $CONC_HIGH -o $DATFILE \
    --camkii $CAMKII --pp1 $PP1 --volume $VOLUME
done
