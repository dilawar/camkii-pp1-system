#!/bin/bash
set -e
set -x
#unset PYTHONPATH
echo `python -c "import moose; print moose.__file__"`
mkdir -p _images
MODEL=camkii_pp1_scheme.py
echo "++ Running $MODEL"
##if [ $# -lt 1 ]; then 
##    python $MODEL -h
##    exit
##fi
CAMKII=20
PP1=26
VOLUME="1e-20"
SIMTIME=`echo "10*24*3600" | bc`
# NOTE: The basal concentration is 80 nano-molar which can go upto 160 nm in
# fluctuations. For these values, I should change my model.
CONC_LOW="0.000080"
CONC_HIGH="0.00016"
DATFILE="camkii_vol${VOLUME}_camkii${CAMKII}_pp${PP1}_ca${CONC_HIGH}.dat"
for vfactor in {1..10}; do 
    VOL="${vfactor}e-19"
    echo "Calculating in volume $VOL"
    time python ./camkii_pp1_scheme.py -st $SIMTIME -cl $CONC_LOW -ch $CONC_HIGH -o $DATFILE \
        --camkii $CAMKII --pp1 $PP1 --volume $VOL
done
