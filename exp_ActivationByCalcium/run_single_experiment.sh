#!/usr/bin/env bash

# CaMKII activation and calcium dynamics at much higher time resolution.

# CaMKII and PP1 are distributed among NUMVOXELS.
RECORD_DT=0.01
DIFFDICT="{ 'x' : 5e-13, 'y' : 5e-13, 'PP1' : 5e-13 }"
SIMTIME=0.01
CAMKII=10
PP1=100
NUMVOXELS=3
PERIOD=500
CA_EXPR="(t>50 && t<52)?1e-3+(0.5e-3*rand2(-1,1,0)):100e-6"

outfile="CaM${CAMKII}_PP${PP1}_voxels${NUMVOXELS}_suOFF.dat"
python ../camkii_pp1_scheme.py --camkii $CAMKII --pp1 $PP1 \
    --simtime $SIMTIME \
    --record-dt $RECORD_DT \
    --ca-expr "$CA_EXPR" \
    --num-voxels $NUMVOXELS \
    -o ${outfile}

python ./analyze.py -i ${outfile}

outfile="CaM${CAMKII}_PP${PP1}_voxels${NUMVOXELS}_suON.dat"
python ../camkii_pp1_scheme.py --camkii $CAMKII --pp1 $PP1 \
    --simtime $SIMTIME \
    --record-dt $RECORD_DT \
    --diff-dict "$DIFFDICT" \
    --ca-expr "$CA_EXPR" \
    --num-voxels $NUMVOXELS --enable-subunit-exchange  \
    -o ${outfile}

python ./analyze.py -i ${outfile}

echo "Analysing the experiment"
python ./analyze_exp.py -c ${CAMKII} -p ${PP1} -t $PERIOD -dt $RECORD_DT
