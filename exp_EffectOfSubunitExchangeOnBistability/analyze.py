#!/usr/bin/env python3
"""
analyze.py: 
"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
import re
import hashlib
import pickle
import scipy.optimize as sco
import helper

camkii_ = 12

def sigmoid(x, x0, k, a):
    x = np.array( x )
    y = a / (1 + np.exp(k*(x-x0)))
    return y

def getData( files ):
    global camkii_
    txt = ''.join(files)
    m = hashlib.md5(txt.encode())
    pickleFile = '%s.pickle' % m.hexdigest()
    if os.path.isfile( pickleFile ):
        print( f"[INFO ] Reading from {pickleFile} pickle" )
        with open( pickleFile, 'rb') as f:
            return pickle.load(f)
    data = []
    for f in files:
        pp1 = int(re.search( 'PP(\d+)', f ).group(1))
        r = pp1 / camkii_
        data.append( (r, pp1, pd.read_csv(f, sep=' ')) )

    with open(pickleFile, 'wb' ) as f:
        pickle.dump(data, f)
    return data

def analyze( dfs, ax1, ax2, color, **kwargs ):
    X, Y, tTrans = [], [],[]
    relaxMean, relaxErr = [], []
    summary = pd.DataFrame()
    for r, pp1, df in sorted(dfs):
        X.append(r)
        tvec = df['time'].values
        y = df['CaMKII*'].values
        h, b = np.histogram( y, range=(0,camkii_+1), bins=camkii_+1, density=True)
        Y.append(sum(h[-4:]))
        tTrans.append( sum( h[2:-4] ) )
        tIndex = helper.transitions_from_midval( y, 6 )
        relaxT = [ tvec[int(b)]-tvec[int(a)] for (a,b) in tIndex ]

        if len(relaxT) > 0:
            relaxMean.append(np.mean(relaxT))
            relaxErr.append(np.std(relaxT))
        else:
            relaxMean.append(0)
            relaxErr.append(0)

    # Raw data
    ax1.plot( X, Y, 'o', color=color, label = kwargs.get( 'label', 'NA' ))
    ax1.plot( X, tTrans, ':', color=color )
    summary['pp_camkii_ration'] = X
    summary['residence_time_ON_state'] = Y
    summary['time_spend_in_transitions' ] = tTrans
    summary['relax_time_mean'] = relaxMean
    summary['relax_time_std'] = relaxErr

    # Fit
    try:
        popt, pcorr = sco.curve_fit( sigmoid, X, Y, p0=[ 12, -0.7, 1 ] )
        x0, k, a = popt
        ax1.plot( X, sigmoid(X, *popt), color=color
                , label = f'Fit: ${a:.2f}/(1+exp({k:.2f}(x-{x0:.2f}))$'
                )
    except Exception as e:
        print( "[WARN ] Failed to compute fit." )
        

    ax1.legend( fontsize=8 )
    ax1.set_xlabel( r'PP1/CaMKII' )
    ax1.set_ylabel( 'Time spent in ON state' )

    ax2.errorbar( X, relaxMean, yerr = relaxErr
            , color=color, label=kwargs.get('label','NA')
            )
    ax2.legend()
    ax2.set_xlabel( 'PP1/CaMKII' )
    ax2.set_ylabel( 'Relaxation time' )
    if 'outfile' in kwargs:
        summary.to_csv( kwargs['outfile'], index = False )
        print( f"[INFO ] Saved to %s" % kwargs['outfile'] )


def main():
    suOFFfiles = glob.glob( f'CaM{camkii_}*_suOFF.dat_processed.dat' )
    suONfiles = glob.glob( f'CaM{camkii_}*_suON.dat_processed.dat' )
    print( "[INFO ]  %d files OFF %d ON" % (len(suOFFfiles), len(suONfiles)))
    n = min(len(suONfiles), len(suOFFfiles))
    offData = getData( suOFFfiles[:n] )
    print( 'Got OFF data' )
    onData = getData( suONfiles[:n] )
    print( 'Got ON data' )

    plt.figure( figsize=(8,4) )
    ax1 = plt.subplot( 121 )
    ax2 = plt.subplot( 122 )
    analyze(offData, ax1, ax2, 'red', label = '-SE', outfile=f'{camkii_}suOFF-summary.csv')
    analyze(onData, ax1, ax2, 'blue', label = '+SE', outfile=f'{camkii_}suON-summary.csv' )
    plt.tight_layout()
    plt.savefig( f'summary_{camkii_}.png' )

if __name__ == '__main__':
    main()
