#!/usr/bin/env /usr/bin/python
"""plot.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def main():
    df = pd.read_csv( sys.argv[1], sep = ' ' )
    plt.plot( df['Time'] , df['Val'] )
    plt.xlabel( 'Time' )
    plt.ylabel( 'Co-localization' )
    plt.savefig( '%s.png' % sys.argv[1] )

if __name__ == '__main__':
    main()
