#!/usr/bin/env python

"""analyze.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2015, Dilawar Singh and NCBS Bangalore"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import matplotlib as mpl

# So I can plot it without running X. Must for batch processing.
mpl.use( 'Agg' )

import os
import numpy as np
import pandas
import scipy
import sys
import datetime
import csv
import re
import subprocess
# import transitions
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

stamp = datetime.datetime.now().isoformat()

try:
    mpl.style.use( 'ggplot' )
except Exception as e:
    print( 'Warning: could not load style %s' % e )
    print( mpl.style.available )
    pass

mpl.rcParams['axes.linewidth'] = 0.1

try:
    gitHash = subprocess.check_output(
            ["git", "log", "--pretty=format:'%h'", "-n", "1" ]
            )
except Exception as e:
    gitHash = 'Unknown'

startN_ = 0
finalN_ = 0

binW_ = 1

now_ = datetime.datetime.today().ctime()
title_ = 'Git\# %s, @ %s' % ( gitHash, now_ )

timeUnit = 'hour' 
if timeUnit == 'year':
    scaleT_ = 1.0 / (365*24*3600)
elif timeUnit == 'month':
    scaleT_ = 1.0 / (30*24*365)
elif timeUnit == 'day':
    scaleT_ = 1.0 / (24*3600.0)
elif timeUnit == 'hour':
    scaleT_ = 1.0 / (3600.0)
else:
    timeUnit = 'second'
    scaleT_ = 1.0


def get_header( data_file ):
    with open( data_file, "r" ) as f:
        header = f.readlines(1)[0].strip()
    return [ x.replace('"', '') for x in  header.split(' ') ]

def analyze( tables ):
    vectors = {}
    for k in tables:
        vectors[k] = tables[k].vector

def merge_neighbours( vec, ignore_radius = 300):
    if len(vec) < 1:
        return []
    newVec = [ vec[0] ]
    for i, v in enumerate( vec[1:] ):
        if v - vec[i] > ignore_radius:
            newVec.append( v )
    return newVec

def get_overlapping_intervals( intevals ):
    xs, ys = [], []
    for x, y in intevals:
        if y not in ys:
            xs.append(x); ys.append(y)
    return zip(xs, ys)

def find_intervals( low_ps, high_ps ):
    intervals = []
    for lowP in low_ps:
        # print('[DEBUG] Checking for interval starting at %s' % lowP )
        highPs = filter(lambda x: x > lowP, high_ps)
        if highPs:
            intervals.append((lowP, highPs[0]-1))
        else:
            intervals.append((lowP, finalN_))
    return get_overlapping_intervals(intervals)

def get_points_where( array, val, distance = 1 ):
    """ Get indices I where  for every i in I, array[i] == val 

    Also remove indices which are separated by 'distance'.
    """
    ps, = np.where( array == val )
    if len(ps) == 0:
        return []
    newps = [ ps[0] ]
    for i, p in enumerate(ps[1:]):
        if p - ps[i] > distance:
            newps.append( p )
    return newps

def find_transitions( vec ):
    """
    Find transitions in given vec. 
    """
    pass 


def plot_histogram( ax, data, label, normed = False ):
    try:
        ax.hist( data, bins = min( 20, data.max() + 1), normed = normed )
    except Exception as e:
        print( 'Failed to plot histogram %s' % label )
        print( '\tError was %s' % e )
    ax.set_title( label )

def smooth( data, window_size ):
    window = np.ones( window_size ) / window_size 
    return np.convolve( data, window, 'same' )

def computeHighIntervals( tvec, yvec ):
    thresH = yvec.max( ) / 2.0
    thresL = 1
    startT, endT = 0, 0
    intervalStarted = False
    slices = [ ]
    # yvec = smooth( yvec, 500 )
    for i, v in enumerate( yvec ):
        if not intervalStarted and v > thresH:
            startT = tvec[ i ]
            intervalStarted = True
        if intervalStarted and v < thresH:
            intervalStarted = False
            endT = tvec[i]
            slices.append( ( startT, endT ) )
    return slices


    
def process( data, args ):
    global startT_, finalN_
    print('[INFO] Processing data for low-camkii')
    # pdf_ = PdfPages( args.output or '%s.pdf' % args.input ) 
    outfile = args.output or '%s.png' % args.input 
    fig = plt.figure( figsize=(8,8)  )
    ax1 = plt.subplot2grid((5,3), (0,0), colspan=3)

    # On large data set following may produce runtime warning.
    # ax1.xaxis.set_minor_locator( ticker.MultipleLocator( 1 ) )

    ax2 = plt.subplot2grid((5,3), (1,0), colspan=3)
    ax3 = plt.subplot2grid((5,3), (2,0), colspan=3)
    ax4 = plt.subplot2grid((5,3), (3,0), colspan=3)
    ax51 = plt.subplot2grid((5,3), (4,0), colspan=1)
    ax52 = plt.subplot2grid((5,3), (4,1), colspan=1)
    ax53 = plt.subplot2grid((5,3), (4,2), colspan=1)


    # Frist drop any spurious colum with all NaN 
    data = data.dropna( 1, 'all' )
    # Drop any row with one or more NaN values.
    data = data.dropna( )
    # Drop first row in any case.
    data.drop( data.index[[0]], inplace=True )

    # Always drop index 0 row from dataframe.
    data.drop( data.index[[0]], inplace=True )

    tVec, camkii0, camkii1 = data['time'], [], []
    tVec = np.array( tVec ) * scaleT_
    finalN_ = len(tVec) - 1

    trans = []
    lowCaM6, lowCaM7 = [], []
    camkii6, camkii7 = [], []
    allCaMKII = [ ]
    ca = []
    for k in data.columns:
        m = re.search( r'x(\d)y(\d)', k )
        if m:
            x = int( m.group( 1 ) )
            y = int( m.group( 2 ) )
            allCaMKII.append( data[k] )
            if x + y == 6:
                camkii6.append( data[ k ] )
            elif x + y == 7:
                camkii7.append( data[ k ] )

        if re.search( r'x0y6(\[\d+\])?\.N', k):
            lowCaM6.append( data[k] )
        if re.search( r'x0y7(\[\d+\])?\.N', k):
            lowCaM7.append( data[k] )
        if re.search( r'(x6y0|x7y0)(\[\d+\])?\.N', k):
            camkii1.append( data[k] )
            # ax2.plot( tVec, data[k], label = k, alpha=0.6 )
        if re.search( r'ca(\[\d+\])?\.N', k):
            ca.append( data[k] )

    lowCaMKII = np.sum( lowCaM6, axis = 0 ) + np.sum( lowCaM7, axis=0 )
    highCaMKII = np.sum( camkii1, axis = 0 )

    try:
        highCaMKII = highCaMKII.values
    except Exception as e:
        pass

    allCaMKII = np.sum( allCaMKII, axis = 0 )
    
    ax1.plot( tVec, highCaMKII, label = 'Active CaMKII' )
    ax1.set_xlabel( 'Time (%s)' % timeUnit )
    ax1.set_title( 'Active CaMKII' )

    # Plot sym6 and sym7 plot on ax2
    allCaMKII6 = np.sum( camkii6, axis=0 )

    try:
        allCaMKII7 = np.sum( camkii7, axis=0)
        ax2.plot( tVec, allCaMKII6, alpha=0.7, label = "All CaMKII(6)" );
        ax2.plot( tVec, allCaMKII7, alpha=0.7, label = "All CaMKII(7)" );
        ax2.set_title( 'Avg. ratio of CaMKII(7) and CaMKII(6) = %f' % 
                ( float(np.sum( allCaMKII7)) / np.sum( allCaMKII6 ) ) 
                )
        ax2.legend(loc='best', framealpha=0.4)
    except Exception as e:
        print( 'Could not plot CaMKII(6) and CaMKII(7)' )
        print( '\tError was %s' % e )


    # CaMKII* is on 53
    plot_histogram( ax53, highCaMKII, 'CaMKII*', normed = True )

    camkiiAll0 = np.sum( camkii0, axis = 0 )
    print( 'Total %d compartment(s) with camkii' % len( camkii0 ))
    camkiiAllLow = np.sum( camkii0, axis = 0 )
    camkiiAllHigh = np.sum( camkii1, axis = 0 )

    print( '[INFO] Total camkii: min=%s, max=%s, avg=%s' % ( 
        camkiiAll0.min(), camkiiAll0.max(), camkiiAll0.mean( ) )
        )
    pp1, camPP1Cplx = [ ], [ ]
    xs, ys = [ ], [ ]

    for k in data.columns:
        if re.search( r'PP1(\[\d+\])?\.N', k):
            p = data[k]
            pp1.append( p )
        elif re.match( r'x(\[\d+\])?\.N', k):
            xs.append( data[k] )
        elif re.match( r'y(\[\d+\])?\.N', k):
            ys.append( data[k] )


    pp1 = np.sum( pp1, axis = 0 )
    try:
        ax3.plot( tVec, np.sum( ca, axis=0) , '-,', label = 'Ca', alpha = 0.6 )
        # ax3.plot( tVec, pp1 , '-,', label = 'PP1', alpha = 0.6 )
        ax3.legend(loc='best', framealpha=0.4)
    except Exception as e:
        print( '[INFO] Failed to plot PP1 %s' % e )

    try:
        ax4.plot( tVec, np.sum(lowCaM6, axis=0), label = 'LOW CaMKII 6'
                , alpha = 0.8 )
        ax4.plot( tVec, np.sum(lowCaM7, axis=0), alpha = 0.8
                , label = 'LOW CaMKII 7' )
        ax4.legend( framealpha = 0.5 )
    except Exception as e:
        print( "Subunit exchange is not enabled. Probably" )
        print( "\t Error was %s" % e )

    plot_histogram( ax51, np.sum(xs, axis=0), 'x' )
    plot_histogram( ax52, np.sum(ys, axis=0), 'y' )
    plt.suptitle( os.path.basename( args.input ), fontsize = 6, x = 0.5, y = 0.02)
    plt.tight_layout( )
    plt.savefig( outfile )
    print( 'Saved to %s' % outfile  )

    #  Compute the interval for which CaMKII stays high.
    intervals = computeHighIntervals( tVec, highCaMKII )
    with open( "%s_results.txt" % args.input , "a" ) as f:
        f.write( "# Following entry was written on: %s\n" % stamp )
        f.write( "# camkii,pp1,start_on,end_on,interval\n" )
        for (starT, endT) in intervals:
            f.write( "%d,%d,%f,%f,%f\n" % ( max(allCaMKII), max(pp1)
                , starT, endT, endT - starT ) 
                )
    
def read_data( args ):
    filename = args.input
    ext = filename.split('.')[-1]
    data = {}
    header = get_header( filename )
    data = pandas.read_csv( filename, comment='#', delimiter = ' ' )
    return data


def main( args ):
    data = read_data( args )
    process( data, args)

if __name__ == '__main__':
    import argparse
    # Argument parser.
    description = '''Argument parser'''
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--input', '-i'
        , required = True
        , help = 'Input file'
        )
    parser.add_argument('--output', '-o'
        , required = False
        , default = False
        , help = 'Output file'
        )
    parser.add_argument( '--debug', '-d'
        , required = False
        , default = 0
        , type = int
        , help = 'Enable debug mode. Default 0, debug level'
        )
    parser.add_argument('--title', '-title'
        , default = title_
        , help = 'Title of the plot'
        )
    class Args: pass 
    args = Args()
    parser.parse_args(namespace=args)
    main( args )
