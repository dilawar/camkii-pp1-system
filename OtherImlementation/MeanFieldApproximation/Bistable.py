"""Bistable.py: 

A bistable switch with given parameters.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import numpy as np
import random

dt_ = 0.5

class BistableLangevian():
    """Bistable described as langevian"""

    def __init__(self, id_, **kwargs):
        self.id = id_
        self.options = kwargs 
        self.high = kwargs.get( 'high', 1 )
        self.low = kwargs.get( 'low', -1 )
        self.m = (self.high - self.low) / 2.0
        self.c = (self.low + self.high ) / 2.0
        self.x = kwargs.get( 'init', self.low )
        self.states = [ self.x ]

    def __str__( self ):
        res = 'Bistable %s' % self.id
        res += str( self.options )
        return res

    def steps( self, n ):
        [ self.step( ) for i in range( n ) ]

    def dx( self, x = None, with_noise = True ):
        e = 0.0
        if with_noise:
            e = random.gauss(0, 0.5 )

        if x is None:
            x = self.x

        s = ( x - self.c ) / self.m
        dx = s - s**3.0 + e
        return dx

    def step( self, theta = 0, ns = [ ] ):
        global dt_
        coupling = 0.0
        if ns:
            coupling = theta / self.m * ( np.mean(ns) - self.x )

        # Following convolution expression instead of dy/dt = y-y^3+et it to
        # rescale the switch from -1 to 1, to 0, k. i.e. x = k/2*(y + 1).
        try:
            dx = self.dx( ) + coupling
        except Exception as e:
            raise e

        self.x += dt_ * self.m * dx
        self.states.append( self.x )

    def plot_bistable( self, ax1, ax2 = None ):
        ax1.plot( np.arange( len(self.states)), self.states )
        ax1.set_xlabel( 'Step' )

        if ax2 is None:
            return 
        try:
            ax2.hist( self.states, normed=True )
            ax2.set_xlabel( 'States' )
        except Exception as e:
            print( 'failed to plot histogram' )


def test( ):

    import matplotlib as mpl
    import matplotlib.pyplot as plt

    try:
        mpl.style.use( 'seaborn-talk' )
    except Exception as e:
        pass
    mpl.rcParams['axes.linewidth'] = 0.1
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    a = BistableLangevian( 1, high = 10, low = 0 )

    a.steps( 10000 )

    plt.figure( figsize=(6,4) )
    gridSize = (2, 3)
    ax1 = plt.subplot2grid( gridSize, (0,0), colspan = 2 )
    ax2 = plt.subplot2grid( gridSize, (0,2), colspan = 1 )
    ax3 = plt.subplot2grid( gridSize, (1,0), colspan = 2 )

    a.plot_bistable( ax1, ax2 )
    ax1.set_title( r'$\dot{x}=x-x^3+\epsilon$' )

    xs = np.linspace( 0, 10, 1000 )
    ys = list( map( lambda x: a.dx( x, with_noise = False ), xs ) )
    ax3.plot( xs, ys )
    ax3.set_title( r'$\dot{x}=x-x^3$' )
    ax3.set_xlabel( 'x' )

    plt.tight_layout( )
    outfile = 'test_bistable.png' 
    plt.savefig( outfile )
    print( 'Saved to %s' % outfile )

if __name__ == '__main__':
    test()
