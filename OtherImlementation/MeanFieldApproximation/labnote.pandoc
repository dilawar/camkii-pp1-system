---
title: Synchronization among coupled noisy CaMKII/PP1 bistable
author: Dilawar Singh
institute: NCBS Bangalore
email: dilawars@ncbs.res.in
geometry: right=5cm, marginparwidth=4cm
date : \today 
header-includes:
    - \usepackage{pgfplots}
    - \usepackage{libertine}
    - \usepackage{mathpazo}
---

# Aim

In our CaMKII/PP1 study, we see that 3 switches coupled via subunit exchange
always synchronises.

![Without subunit exchange](./_images/3voxels.png){#fig:without_su width=6cm} 
![With subunit exchange.](./_images/3voxels_su.png){#fig:with_su width=6cm} 


__On left__ No subunit exchange (no coupling). We see the binomial distribution.
__On right__ Coupling among switches is due to subunit exchange. All switches
synchronises and acts as one bistbale switch.

Is there a simple abstract model of bistable switch which reproduce synchronises
among N bistable switches?

# Model

The following Langevian equation describes a bistbale. Figure @fig:langevian_bis
shows a sample trajectory.

$$\dot x = x - x^3 + \sqrt{2D}{\epsilon t}$$ {#eq:bis_without_coupling}

![A sample solution of equation @eq:bis_without_coupling](./_images/lang_bis.png){#fig:langevian_bis width=10cm}


## Interacting bistables

A bistable recieving input from other bistables can 'influence' its state x.
The 'coupling parameter'  is written as $\theta$.

If a switch is interacting with $n$ other switches $x_1,x_2,\ldots x_n$ with
interaction $\theta$ then this system can be described by @eq:coupled_bis.

$$\dot x = x - x^3 + \sqrt{2D}{\epsilon t} + \frac{\theta}{n}\left({\sum x_i - n
x}\right)$$ {#eq:coupled_bis}

To restrict the bistable to range $(a,b)$, we use the transformation $y=mx+c$
where $m=\frac{b-a}{2}$ and $c=\frac{a+b}{2}$.

__Coupled bistables on ring__

For n bistables connected with nearest neighbour (each one is connected with two
neighbours ), we  have the following equation.

$$\dot x = x - x^3 + \sqrt{2D}{\epsilon t} + \frac{\theta}{2}\left(x_{i-1} 
+ x_{i+1} - 2x_i\right)$$ {#eq:coupled_bis_3}

This model fails to generate 'synchronous' behaviour shown in @fig:coupled_bis_no_sync .

![Three coupled noisy bistables with different coupling coefficient described by
@eq:coupled_bis_3 . At high value of theta, we see some synchronizing behaviour. 
](./_images/coupled_bis_1.png){#fig:coupled_bis_no_sync width=15cm}

_More on it is discussed in [this paper](http://iopscience.iop.org/article/10.1209/0295-5075/88/40006/fulltext/)
`doi:10.1209/0295-5075/88/40006`._

__Globally coupled bistables__

If subunits from each switch are stored in one pool, then we have a mean-field
model. Since the number of subunits in global pool are proportional to the state
of switch, the average number of subunits are proportional to the average state
of all switches $\bar{x}$ where $\bar{x} = \sum_n{x_i} / n$.

$$\dot x = x - x^3 + \sqrt{2D}{\epsilon t} + \theta \left(\bar{x} -
x\right)$${#eq:coupled_bis_globally}

In this configuration, we see the synchronization of switches as shown in
@fig:bistables_sync_3 .

![ Synchronization of switches in system described by @eq:coupled_bis_globally. 
](./_images/coupled_bis_sync.png){#fig:bistables_sync_3 width=15cm}


