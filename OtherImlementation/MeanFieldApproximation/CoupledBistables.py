"""CoupledBistables.py: 2
    Coupled bistables.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import random

import numpy as np
import networkx as nx
import Bistable

import matplotlib as mpl
import matplotlib.pyplot as plt
try:
    mpl.style.use( 'seaborn-talk' )
except Exception as e:
    pass

mpl.rcParams['axes.linewidth'] = 0.2
mpl.rcParams['lines.linewidth'] = 1.0

plt.rc('text', usetex=True)


class CoupledBistables( ):

    def __init__(self, n, theta = 0.1, **kwargs):
        print( 'Creating ring topology' )
        self.g = nx.cycle_graph( n, nx.Graph( ) )
        self.high = kwargs.get( 'high', 1 )
        self.low = kwargs.get( 'low', -1 )
        self.theta = theta
        self.init( )

    def init( self ):
        for n in self.g:
            print( ' Node %s' % n )
            s = int( 10 * random.random( ) )
            self.g.node[ n ]['bistable'] = Bistable.BistableLangevian( n
                    , high = self.high, low = self.low
                    )

    def step( self, coupling = 'global' ):
        if coupling == 'nearest_neighbour':
            for n in self.g:
                ns = self.g.neighbors( n ) 
                nsX = [ self.g.node[a]['bistable'].x for a in ns ]
                self.g.node[ n ]['bistable'].step( self.theta, nsX )
        elif coupling == 'global':
            xis = [ self.g.node[a]['bistable'].x for a in self.g.nodes( ) ]
            for n in self.g:
                self.g.node[ n ]['bistable'].step( self.theta, xis )
        else:
            print( 'Coupling not known' )
            quit( 1 )

    def steps( self, n ):
        print( 'Simulating for %d steps' % n )
        for i in range( n ):
            self.step( )

    def plot( self, ax1, ax2 = None, **kwargs ):

        bis = [ self.g.node[ n ]['bistable'].states for n in self.g ]
        res = np.sum( bis, axis = 0 )
        npoints = kwargs.get( 'points', len( res ) )

        offset, yticks = 0, [ ]
        for i, b in enumerate( bis ):
            yticks.append( offset )
            b = np.array( b )
            ax1.plot( offset + b[:npoints])
            offset += max( b ) + 1
        offset += 0.5
        yticks.append( offset )

        ax1.set_yticks( yticks )
        ax1.set_yticklabels( list(range( 0, len(bis))) + [ 'All' ] )
        ax1.set_ylabel( r'\# Bistble' )
        ax1.plot( offset + res[:npoints], lw = 0.3 )

        if ax2 is None:
            return 

        try:
            ax2.hist( res, normed = True )
        except Exception as e:
            print( 'Failed to plot histogram' )
            print( ' Error %s' % e )

def test( ):
    a = CoupledBistables( 3,  theta = 0.3, high = 10, low = 0 )
    a.steps( 50000 )
    plt.figure( figsize=(6,2) )
    gridSize = (1, 3)
    ax1 = plt.subplot2grid( gridSize, (0,0), colspan = 2 )
    ax2 = plt.subplot2grid( gridSize, (0,2), colspan = 1 )
    a.plot( ax1, ax2 )

    plt.tight_layout( )
    plt.savefig( 'test_coupled_bis.png' )

if __name__ == '__main__':
    test()
