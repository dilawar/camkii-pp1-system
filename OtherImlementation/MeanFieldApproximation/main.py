"""main.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import CoupledBistables

def main( ):

    thetas = np.linspace( -0.3, 0.3, 5 )
    gridSize = ( len(thetas), 3 )

    plt.figure( figsize=(6, 1.5*len(thetas) ) )

    for i, theta in enumerate( thetas ):
        print( 'Simulating for theta %s' % theta )
        a = CoupledBistables.CoupledBistables( 3, theta = theta )
        a.steps( 50000 )

        ax1 = plt.subplot2grid( gridSize, (i,0), colspan = 2 )
        plt.title( 'Theta=%s' % theta )

        ax2 = plt.subplot2grid( gridSize, (i,2), colspan = 1 )
        a.plot( ax1, ax2, points = 10000 )

    plt.tight_layout( )
    outfile = 'coupled_bis.png' % theta
    plt.savefig( outfile )
    plt.close( )

if __name__ == '__main__':
    main()
