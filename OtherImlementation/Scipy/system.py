#!/usr/bin/env python3
"""system.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import os
import random

import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
mpl.style.use( 'ggplot' )
import scipy.integrate as sci
from params import *

cavec_  = [ ]

def ca( t ):
    cavec_.append( ca_basal if t%4<2 else ca_basal*random.uniform(1,2) )
    return cavec_[-1]

def v1( t, ca ):
    frac = ( ca / K_H1 ) ** 3
    return (frac/(1+frac)) ** 2.0

def v2( t, ca ):
    frac = ( ca / K_H1 ) ** 3
    return (frac/(1+frac)) 

def f( t, y ):
    x6y0, x4y2, x0y6, pp = y
    _ca = ca(t)
    _v1, _v2 = v1(t, _ca), v2(t, _ca)
    dx6y0 = - _v1 * x6y0
    dx4y2 = _v1 * x6y0 - _v2 * x4y2
    dx6y0 = _v2 * x6y0
    return [ dx6y0, dx4y2, dx6y0 ]

def main( ):
    t0, y0 = 0, [1e-3, 0, 0, 1e-3 ]
    r = sci.ode(f).set_integrator('lsoda' )
    r.set_initial_value(y0, t0)
    t1 = 100
    dt = 1
    ts, ys = [ ], [ ]
    while r.successful() and r.t < t1:
        ts.append( r.t )
        ys.append( r.integrate( r.t + dt ) )

    ys = np.array( ys )
    plt.subplot( 211 )
    plt.plot( ts, ys[:,2] )
    plt.subplot( 212 )
    plt.plot( cavec_ )
    output = 'output.png'
    plt.savefig( output )
    print( 'Saved to %s' % output )

if __name__ == '__main__':
    main()
