"""System.py: 

"""
    
__author__           = "Me"
__copyright__        = "Copyright 2016, Me"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Me"
__email__            = ""
__status__           = "Development"

import random
import math
import Rate
import Globals as _g
from Base import Reac

class System( ):

    def __init__( self, states, args = [ ] ):
        self.args = args 
        self.dt = _g.getTick( 'System' )
        self.time = 0.0
        self.reacs = { }
        # Reaction list in which a molecule is involed as substrate.
        self.reaclist = {}
        self.species = { }
        self.propensities = { }
        self.dynamicSpecies = [ ]
        for x in states:
            self.species[ x.name ] = x
            self.reaclist[ x.name ] = []
        self.trajectory = [ ]
        self.build( )
        self.next = 0.0
        self.alpha = _g.eps_


    def add_reac( self, **rdict ):
        """Add a reaction
        """


        name = rdict[ 'name' ]

        # print( 'Adding reaction %s\n | %s' % (name, rdict) )
        if rdict.get( 'kf', False ):
            forwardReac = Reac( name + '_forward' )
            forwardReac.rate = Rate.Rate( rdict[ 'kf' ], self.species )
            for s in rdict[ 'subs' ]:
                forwardReac.subs.append( s )
                forwardReac.stoich[ s ] = forwardReac.stoich.get(s, 0) - 1

            for p in rdict[ 'prds' ]:
                forwardReac.prds.append( p )
                forwardReac.stoich[ p ] = forwardReac.stoich.get(p, 0) + 1

            self.reacs[ forwardReac.name ] = forwardReac
            self.propensities[ forwardReac.name ] = 0.0

        if rdict.get( 'kb', False):
            backwardReac = Reac( name + '_backward' )
            backwardReac.rate = Rate.Rate( rdict[ 'kb' ], self.species )
            for s in rdict[ 'subs' ]:
                backwardReac.prds.append( s )
                backwardReac.stoich[ s ] = backwardReac.stoich.get(s, 0) + 1

            for p in rdict[ 'prds' ]:
                backwardReac.subs.append( p )
                backwardReac.stoich[ p ] = backwardReac.stoich.get(p, 0) - 1

            self.reacs[ backwardReac.name ] = backwardReac
            self.propensities[ backwardReac.name ] = 0.0

        rtext = name  + ' : ' + ' + '.join( rdict['subs'] ) 
        rtext += ' <- %s, %s -> ' % ( rdict.get( 'kf' ), rdict.get( 'kb' ) )
        rtext += ' + '.join( rdict[ 'prds' ] ) 
        print( rtext )

    def buildDependenciesAmongReactions( self ):
        print( 'Building reaction dependencies' )
        for r in self.reacs:
            reac = self.reacs[r]
            for s in reac.subs:
                self.reaclist[s].append( reac )
        print( '\tDone %s' % self.reaclist )

    def build( self ):
        for reac in self.args:
            self.add_reac( reac )


    def reinit( self ):
        """Initialize the system. Compute when first reaction should happen """
        for s in self.species.values( ):
            if s.dynamic:
                self.dynamicSpecies.append( s )

        print( '[INFO] Total dynamic species %s' % self.dynamicSpecies )
        self.buildDependenciesAmongReactions( )

        for r in self.reacs:
            self.reaction_compute_next( self.reacs[r] )

        print( 'System is initialized' )

    def do_reaction( self, reac ):
        """Do this reaction.

        If any change caused by this reaction triggers another reactions compute
        its time as well.
        """
        # print( 'Doing reaction @ %f, %s' % (self.time, reac ) )
        if not reac:
            return False
        for s in reac.subs: 
            # For this subtrate, trigger all reactions which have this molecule
            # as substrate
            self.species[ s ].increment( reac.stoich[ s ] )

        for p in reac.prds:
            self.species[ p ].increment( reac.stoich[ p ] )

        return True

    def reaction_compute_next( self, reac ):
        propensity =  reac.rate.getVal( self.time )
        for s in reac.subs:
            n = self.species[s].getN( )
            if n == 0.0:
                # self.next_ = self.time + self.dt
                return False
            propensity *= n ** (- reac.stoich[ s ])

        # Update the reac.next_. There is chance of introducing error by
        # choosing the large dt.
        if self.time <= reac.next_:
            return False
        reac.next_ = self.time + math.log( 1.0 / random.random( ) ) / propensity 
        # print( '\t %s step %s' % (reac, reac.next_ ) )
        return True

    def save_step( self ):
        self.trajectory.append(
                [ self.time ] + [ self.species[x].getN() for x in self.species ] 
                )

    def whichReac( self ):
        r2 = random.random( )
        props = self.propensities.values( )
        for i, reac in enumerate(self.propensities):
            if r2 < sum( props[:(i+1)] ) / self.alpha:
                return self.reacs[reac]
        return False

    def nextReacTime( self ):
        # Compute when next reaction will happen.
        for r in self.reacs:
            reac = self.reacs[r]
            propensity = 1.0
            for s in reac.subs:
                n = self.species[s].getN( )
                propensity *= n ** (- reac.stoich[ s ])
            self.propensities[ r ] = propensity

        self.alpha = max( _g.eps_, sum( self.propensities.values( ) ))
        tau = self.time + math.log( 1.0 / random.random() ) / self.alpha
        return tau

    def doReaction( self ):
        pass

    def solve( self, curtime ):
        self.time = curtime 

        # The algorithm is quite simple. We ask when the next reaction will
        # happen and then we ask which reaction will happen.
        if self.time >= self.next:
            # before computing reactions. Update dynamic species. Note: If we update
            # it when reaction are scheduled, then things will be faster. But we
            # will miss values for plotting.
            for s in self.dynamicSpecies:
                s.update( self.time, self.species )
            self.next = self.nextReacTime( )
            self.do_reaction( self.whichReac( ) )
