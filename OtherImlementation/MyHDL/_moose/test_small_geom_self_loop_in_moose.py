"""test_small_geom.py

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Me"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh <dilawars@ncbs.res.in>"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import moose
import moose.utils as mu

import time
import matplotlib as mpl
import matplotlib.pyplot as plt
try:
    mpl.style.use( 'seaborn-talk' )
except Exception as e:
    pass
mpl.rcParams['axes.linewidth'] = 0.1
plt.rc('text', usetex=True)
plt.rc('font', family='serif')


def main( ):
    cyl = moose.CylMesh( '/cyl' )
    cyl.r0 = cyl.r1 = 0.1e-6
    cyl.x1 = 0.1e-6
    print( 'volume %g' % cyl.volume )

    pools = { }
    tables = { }
    for p in [ 'a', 'b', 'c' ]:
        pools[p] = moose.Pool( '/cyl/%s' % p )
        pools[p].nInit = 0

    pools[ 'a' ].nInit = 10

    for p in pools.values( ):
        t = moose.Table2( '/cyl/table_%s' % p.name )
        moose.connect( t, 'requestOut', p, 'getN' )
        tables[ p.name ] = t

    reacs = {}
    for r in [ 'r1' , 'r2', 'r3' ]:
        reacs[r] = moose.Reac( '/cyl/%s' % r )

    moose.connect( reacs[ 'r1' ], 'sub', pools['a'], 'reac' )
    moose.connect( reacs[ 'r1' ], 'prd', pools['b'], 'reac' )

    moose.connect( reacs[ 'r2' ], 'sub', pools['b'], 'reac' )
    moose.connect( reacs[ 'r2' ], 'prd', pools['c'], 'reac' )

    moose.connect( reacs[ 'r3' ], 'sub', pools['c'], 'reac' )
    moose.connect( reacs[ 'r3' ], 'prd', pools['a'], 'reac' )

    reacs[ 'r1' ].numKf = 1
    reacs[ 'r2' ].numKf = 0.5
    reacs[ 'r3' ].numKf = 0.5

    reacs[ 'r1' ].numKb = 0.0
    reacs[ 'r2' ].numKb = 0.0
    reacs[ 'r3' ].numKb = 0.0

    stoich = moose.Stoich( '/cyl/stoich' )
    s = moose.Gsolve( '/cyl/solve' )
    stoich.ksolve = s
    stoich.compartment = cyl
    stoich.path = '/cyl/##'

    t = time.time( )
    moose.reinit( )
    moose.start(20 )
    print( 'Simulation took %f sec' % ( time.time() - t ) )
    # mu.plotRecords( tables, outfile = './result_moose.png' )
    mu.plotRecords( tables )

if __name__ == '__main__':
    main()

