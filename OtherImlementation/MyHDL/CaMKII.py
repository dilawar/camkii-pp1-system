"""CaMKII.py: 

System of CaMKII

"""
    
__author__           = "Me"
__copyright__        = "Copyright 2016, Me"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Me"
__email__            = ""
__status__           = "Development"

import sys
import os
import random
import myhdl as _h

# Tick for this module.
dt_ = 1e-3

def CaMKIIRing( clk, calcium, pp1, number_of_subunits ):
    """
    CaMKII ring with given number of subunits and update clock

    """

    rings_ = range( number_of_subunits )
    states_ = [ 0 ] * number_of_subunits

    @_h.always( clk.posedge )
    def process( ):
        # Calcium immediately binds to CaMKII and activate fire
        print( 'Current state %s' % states_ )

    return process


def test( ):
    print( 'testing' )
    clk = _h.Signal( bool( 0 ) )

    # These are the number of molecules, CaMKIIRing will see in its immediate
    # proximity on average during dt_
    calcium, pp1 = _h.Signal( 1e-1 ), _h.Signal( 1e-10 )

    @_h.always( _h.delay( 1 ) )
    def clkgen( ):
        clk.next = not clk

    a = CaMKIIRing( clk, calcium, pp1, 6 )

    return clkgen, a

def main( ):
    tb = test( )
    sim = _h.Simulation( tb )
    sim.run( 10 )

if __name__ == '__main__':
    main()
