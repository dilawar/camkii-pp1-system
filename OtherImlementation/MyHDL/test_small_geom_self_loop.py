"""test_small_geom.py

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Me"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh <dilawars@ncbs.res.in>"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
sys.path.append( '..' )
import os
import math
import time
import re
import time
import gnuplotlib 
import numpy as np
import myhdl as _h
import Globals as _g
import System 
import Base 

# _g.setSeed( 100 )

def Cylinder( radius, length, **kwargs ):
    """ A cylinderical compartment. It contains a system and a process. Process
    is called at every tick. System contains stochastic or determinstic system.
    Currently only stochastic system is supported.

    """

    volume = math.pi * radius * radius * length
    print( 'Volume is %g' % volume )

    clk = _h.Signal( bool( 0 ) )
    a = Base.Pool( 'a', volume, n=10 )
    b = Base.Pool( 'b', volume, n=0 )
    c = Base.Pool( 'c', volume, n=0 )

    allPools = [ a, b, c ]
    # Initialize system.
    sys = System.System( allPools )
    
    sys.add_reac( name = 'reac_1', subs = [ "a" ], kf = 1.0 , prds = [ "b" ] )
    sys.add_reac( name = 'reac_2', subs = [ "b" ], kf = 0.5, prds = [ "c" ] )
    sys.add_reac( name = 'reac_3', subs = [ "c" ], kf = 0.5, prds = [ "a" ] )

    # user must reinitialize
    sys.reinit( )

    completed = [ 0 ]

    @_h.always( _h.delay( 1 ) )
    def process( ):
        currtime_ = sys.dt * _h.now( )
        sys.time = currtime_
        sys.solve( currtime_ )
        # if _h.now( ) % 100 == 0:
        sys.save_step( )
        if currtime_ % (24 * 3600) == 0:
            completed[0] = completed[0] + 1
            print( '%d days of simulation is over' % completed[0] )

    return sys, process

def main( ):
    sys, tb = Cylinder( 0.1e-6, 0.1e-6 )
    sim = _h.Simulation( tb )
    t = time.time( )
    sim.run( int( 10.0 / sys.dt ) )
    print( 'Simulation took %f sec' % ( time.time() - t ) )
    np.savetxt( '_data/data.txt', sys.trajectory
            , comments = ''
            , header = 'time' + ' ' + ' '.join( sys.species ) 
            )
    print( '\tOverall %f sec' % ( time.time( ) - t ) )

if __name__ == '__main__':
    main()

