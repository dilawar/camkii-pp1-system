"""spine.py

Compartment is a geometrical unit for chemical computation. This is an
approximation of post synaptic density in spine.
"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Me"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh <dilawars@ncbs.res.in>"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import math
import time
import re
import time
import gnuplotlib 
import numpy as np
import myhdl as _h
import Globals as _g
import System 
import Base 

_g.setSeed( 100 )

def Cylinder( radius, length, **kwargs ):
    """ A cylinderical compartment. It contains a system and a process. Process
    is called at every tick. System contains stochastic or determinstic system.
    Currently only stochastic system is supported.

    """

    volume = math.pi * radius * radius * length
    print( 'Volume is %g' % volume )

    clk = _h.Signal( bool( 0 ) )
    ca = Base.Pool( 'ca', volume, conc='100e-6' ) # *(1+0.5*rand())' )
    p0u6 = Base.Pool( 'p0u6', volume, n=8.0 )
    p1u5 = Base.Pool( 'p1u5', volume, n=0 )

    kh2 = Base.conc_to_n( 0.3e-3, volume )

    # I1P exchange from cytosol dynamics are very fast so we can write down
    # stationary concentrations.
    i1 = Base.conc_to_n( 0.1e-3, volume )
    i1p = Base.Pool( 'I1P', volume
            , n = '{0}*(1 + ca/{1})**3/(ca/{1})**3'.format( i1, kh2 )
            )

    #  This may not be a  good idea. Since we need PP1 in reaction,
    # inactive PP1.
    pp1 = Base.Pool( 'PP1', volume, n=2.0 )
    i1p_pp1 = Base.Pool( 'PP1+I1P', volume, n=0.0 )

    # This complex is formed by CaMKII and PP1
    pp1cplx = Base.Pool( 'PP1Cplx', volume, n = 0.0 )

    allPools = [ ca, p0u6, p1u5, pp1, i1p_pp1, i1p, pp1cplx ]
    for i in range( 2, 7 ):
        allPools.append( Base.Pool( 'p%du%d' % (i, 6-i), volume, n = 0.0 ) )

    # Initialize system.
    sys = System.System( allPools )
    
    # First slow step of CaMKII phosphorylation.
    kh1 = Base.conc_to_n( 0.7e-3, volume )
    sys.add_reac( name = 'reac_0to1' 
            , subs = [ "p0u6" ]
            , kf = '6*1.5*((ca/{0})**6.0)/((1 + (ca/{0})**3)**2)'.format(kh1)
            , prds = [ "p1u5" ] 
            )

    # PP1 and I1P. Ad modelled by Zhabotinsk
    sys.add_reac( name = 'reac_i1p+pp1'
            , subs = [ "I1P", "PP1" ], prds = [ "PP1+I1P" ]
            , kf = 1e5, kb = 1e-5
            )

    # for r in range(1, 2):
        # if r+1 < 7:
            # sys.add_reac( name = 'reac_%dto%d' % (r, r+1)
                    # , subs = [ "p%du%d" % (r, 6-r) ]
                    # , kf = '1.5*((ca/{0})**3.0)/(1 + (ca/{0})**3)'.format(kh1)
                    # , prds = [ "p%du%d" % (r+1,6-r-1) ] 
                    # )

        # sys.add_reac( name = 'reac_%dto%d' % (r+1, r)
                # , subs = [ "p%du%d" % (r, 6-r), 'PP1' ]
                # , kf = 10.0 / Base.conc_to_n( 0.4e-3, volume )
                # , prds = [ "PP1Cplx" ] 
                # )

    # sys.add_reac( name = 'reac_desphospho' 
            # , subs = [ "PP1Cplx" ]
            # , kf = 10.0
            # , prds = [ "PP1", "p0u6" ] 
            # )

    # user must reinitialize
    sys.reinit( )

    completed = [ 0 ]

    @_h.always( _h.delay( 1 ) )
    def process( ):
        currtime_ = sys.dt * _h.now( )
        sys.time = currtime_
        sys.solve( currtime_ )
        if _h.now( ) % 100 == 0:
            sys.save_step( )
        if currtime_ % (24 * 3600) == 0:
            completed[0] = completed[0] + 1
            print( '%d days of simulation is over' % completed[0] )

    return sys, process

def main( ):
    sys, tb = Cylinder( 0.1e-6, 0.1e-6 )
    sim = _h.Simulation( tb )
    t = time.time( )
    sim.run( int( 0.1 * 24 * 3600  / sys.dt ) )
    print( 'Simulation took %f sec' % ( time.time() - t ) )
    np.savetxt( '_data/data.txt', sys.trajectory
            , comments = ''
            , header = 'time' + ' ' + ' '.join( sys.species ) 
            )
    print( '\tOverall %f sec' % ( time.time( ) - t ) )

if __name__ == '__main__':
    main()

