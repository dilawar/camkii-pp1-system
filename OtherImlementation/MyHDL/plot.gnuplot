set terminal png size 1000,1000
set output "__img.png"
#set xlabel "Time"
set multiplot layout 5, 1 title "CaMKII/PP1 System"
set lmargin 10
plot "./_ramdisk/data.txt" using 1:"ca" with lines title columnheader
plot "./_ramdisk/data.txt" using 1:"p0u6" w l , '' using 1:"p2u4" w l title columnheader
plot "./_ramdisk/data.txt" using 1:"PP1" w l title columnheader
plot "./_ramdisk/data.txt" using 1:"I1P" w l title columnheader
plot "./_ramdisk/data.txt" using 1:"PP1Cplx" w l title columnheader
