"""globals.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Dilawar Singh"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import random

eps_ = 1e-9
seed_ = -1

ticks_ = { 'System' : 0.01 }

def getTick( name ):
    return ticks_.get( name )

def setSeed( seed ):
    seed_ = seed 
    random.seed( seed )

def rand( ):
    return random.random( )
