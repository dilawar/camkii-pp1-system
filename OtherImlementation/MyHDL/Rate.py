"""Rate.py: 

Implements Rate class.

"""
    
__author__           = "Me"
__copyright__        = "Copyright 2016, Me"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Me"
__email__            = ""
__status__           = "Development"

import random
import Globals as _g

if _g.seed_ >= 0:
    random.seed( _g.seed )

from math import *

# Use global random number generator.
rand = _g.rand 

class Rate( ):
    """Class for handling rate of reactions """

    def __init__( self, expr, signals = { } ):
        """
        When evaluation, read value from signal
        """
        self.expr = expr
        self.signals = signals
        self.dynamic = False
        try:
            self.val = float( eval( str(expr) ) )
            self.type = float
        except Exception as e:
            self.type = str
            self.dynamic = True
            self.expr = compile( self.expr, '<string>', 'eval' )

    def __float__( self, time = 0 ):
        return self.getVal( time )

    def getVal( self, time, variables = [ ] ):
        """
        Evaluate this rate current time.

        """
        if len( variables ) == 0:
            variables = self.signals.keys( )

        if self.type == float:
            return self.val
        else:
            locals = { 't' : time }
            for s in variables:
                if s != 't':
                    locals[ s ] = self.signals[s].getN( ) 
            return eval( self.expr, locals  )
