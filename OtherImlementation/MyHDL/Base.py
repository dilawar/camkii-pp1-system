"""Reac.py: 

"""
    
__author__           = "Me"
__copyright__        = "Copyright 2016, Me"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Me"
__email__            = ""
__status__           = "Development"

import Globals as _g
import myhdl as _h
from math import *

# Use global random number generator.
rand = _g.rand

def conc_to_n( conc, volume ):
    return conc * 6.023e23 * volume 

class Pool( ):
    """Pool
    """

    def __init__( self, name, volume=1, conc = 1, n = -1 ):
        self.name = name
        self.volume = volume
        self.dynamic = False
        self.conc = 0.0
        self.concOrN = 'n'
        self.state = None
        if type( conc ) != str and type( n ) != str:
            if n > -1:
                self.state = _h.Signal( float(n) )
            else:
                self.conc = float(conc)
                self.state = _h.Signal( conc_to_n( conc, volume ) )
        else:
            self.dynamic = True
            self.expr = n
            if type( conc ) == str:
                self.expr = conc
                self.concOrN = 'conc'
            self.expr = compile( self.expr, '<string>', 'eval' )
            self.state = _h.Signal( _g.eps_ )

    def getN( self ):
        return self.state.val 

    def getConc( self ):
        return self.conc

    def setConc( self, conc ):
        self.conc = conc 
        self.n = conc_to_n( conc, self.volume )

    def increment( self, dn ):
        self.state.next = max( 0.0, self.state.val + dn )
        self.conc = self.state.val / self.volume 

    def update( self, time, variables = { } ):
        assert self.dynamic
        t = time 
        localVs = {}
        for v in variables:
            localVs[v] = variables[ v ].getN( )

        val = eval( self.expr, locals = localVs )
        if self.concOrN == 'conc':
            val = conc_to_n( val, self.volume )
        self.state.next = val
        return True


    def __repr__( self ):
        return '%s (n=%.1f)' % ( self.name , self.state.val )


class Reac( ):
    
    def __init__( self, name ):
        self.name = name
        self.subs = [ ]
        self.prds = [ ]
        self.rate = None
        self.stoich = { }
        self.next_ = 0.0

    def __repr__( self ):
        msg = '%s: ' % self.name 
        msg += ','.join( self.subs )
        msg += ' -> ' + '%g' % self.rate 
        msg += ' -> ' + ','.join( self.prds )
        return msg

