import pandas
import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
try:
    mpl.style.use( 'seaborn-talk' )
except Exception as e:
    pass
mpl.rcParams['axes.linewidth'] = 0.1
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

def plot_all( data ):
    t = data[ 'time' ] / (24 * 3600)
    data = data.drop( [ 'time' ],  1 )
    N = len( data.columns )
    plt.figure( figsize=(10,1.5*N ) )
    for i, c in enumerate( data.columns ):
        plt.subplot( N + 1, 1, i+1)
        plt.plot( t, data[ c ], label = c )
        plt.legend( )

    plt.subplot( N + 1, 1, N + 1 )
    cols = list( set( data.columns ) - set( [ 'ca' ] ) )
    print( cols )
    plt.plot( t, data[cols].sum( axis = 1 ), label = 'sum' )
    plt.legend(loc='best', framealpha=0.4)
    plt.tight_layout( )
    plt.savefig( './__img.png' )
    print( 'Outfile file is written to ./__img.png' )

def main( ):
    data = pandas.read_csv( sys.argv[1], sep = ' ' )
    plot_all( data )

if __name__ == '__main__':
    main()
