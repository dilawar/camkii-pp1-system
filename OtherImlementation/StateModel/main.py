"""main.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import numpy as np
import networkx as nx

t = 0.0

def build_state_model( camkii = 100, pp1 = 50, plot = True ):
    g = nx.DiGraph( )
    g.add_node( 'camkii', Ninit = camkii, N = camkii )
    g.add_node( 'camkii*', Ninit = 0, N = 0 )
    g.add_node( 'camkii**', Ninit = 0, N = 0 )
    g.add_node( 'PP1', Ninit = pp1, N = pp1 )
    g.add_node( 'PP1_', Ninit = pp1, N = pp1 )
    g.add_node( 'ca', Ninit = '1' )

    # Transitions.
    g.add_edge( 'camkii', 'camkii*' )
    g.add_edge( 'camkii*', 'camkii**' )

    # PP1 transitions.
    g.add_edge( 'PP1', 'PP1_' )
    g.add_edge( 'PP1_', 'PP1' )

    if plot:
        import matplotlib as mpl
        import matplotlib.pyplot as plt
        mpl.style.use( 'bmh' )
        mpl.rcParams['axes.linewidth'] = 0.2
        mpl.rcParams['lines.linewidth'] = 1.0
        mpl.rcParams['text.latex.preamble'] = [ r'\usepackage{siunitx}' ]
        mpl.rcParams['text.usetex'] = False
        import matplotlib.pyplot as plt
        nx.draw_networkx( g )
        plt.savefig( 'network.png' )

    return g


def main( ):
    g = build_state_model( )

if __name__ == '__main__':
    main()
