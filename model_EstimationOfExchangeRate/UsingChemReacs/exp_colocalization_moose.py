#!/usr/bin/env python
"""colocalization_moose.py:

Colocalization estimates.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import numpy as np
import moose
import pandas as pd
import helper
import networkx as nx

import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.style.use( 'bmh' )
mpl.rcParams['axes.linewidth'] = 0.2
mpl.rcParams['lines.linewidth'] = 1.0
mpl.rcParams['text.latex.preamble'] = [ r'\usepackage{siunitx}' ]
mpl.rcParams['text.usetex'] = False


species_ = { }
tables_  = { }
args_    = None
txt_     = []

def double_exp(t, tau, a):
    return 100-(a*np.exp(-t/tau) - 100*np.exp(-2*t/tau))

def fit_chain_reac(t, y):
    import scipy.optimize as sco
    popt, pcorr = sco.curve_fit(double_exp, t, y)
    return popt

def add_species( compt, name ):
    global species_
    global tables_
    p = '%s/%s' % (compt.path, name)
    a = moose.Pool( p )
    t = moose.Table2( '%s.tab' % p )
    moose.connect( t, 'requestOut', p, 'getConc' )
    tables_[name] = t
    a.concInit = 0
    species_[name] = a

def reac2str( r ):
    subs = [x.name for x in r.neighbors['sub']]
    prds = [x.name for x in r.neighbors['prd']]
    return ' + '.join(subs) + ' <-%g,%g-> ' %(r.Kb, r.Kf) + ' + '.join(prds)

def add_loose_reac( compt, sub, prd, subunit, kf, kb = 0 ):
    global species_
    rname = '%s/%sto%s' % (compt.path, sub, prd )
    r = moose.Reac( rname )
    subunitIndex = sub.find( subunit )
    nSubunitWhichCanBeLost = int(sub[subunitIndex+1])
    assert nSubunitWhichCanBeLost > 0
    moose.connect( r, 'sub', species_[sub], 'reac' )
    moose.connect( r, 'prd', species_[prd], 'reac' )
    moose.connect( r, 'prd', species_[subunit], 'reac' )
    r.Kf = nSubunitWhichCanBeLost * kf
    r.Kb = kb
    #  print( "[INFO ] LOOSE reaction: %s" % reac2str(r) )

def add_gain_reac( compt, sub, prd, subunit, kf, kb=0):
    global species_
    rname = '%s/%sto%s' % (compt.path, sub, prd )
    r = moose.Reac( rname )
    moose.connect( r, 'sub', species_[sub], 'reac' )
    moose.connect( r, 'sub', species_[subunit], 'reac' )
    moose.connect( r, 'prd', species_[prd], 'reac' )
    r.Kf = kf
    r.Kb = kb
    #  print( "[INFO ] GAIN reaction %s" % reac2str(r) )

def loose_subunit( compt, c ):
    global txt_
    #  print( "[INFO ] %s will loose subunits" % c )
    r, g = int(c[1]), int(c[3])
    assert r + g == 7
    # it can loose both R and G
    if r > 0 and g > 0:
        #  print( '\tLoose both R and G', end = ' ' )
        prds =  'R%dG%d'%(r,g-1), 'R%dG%d'%(r-1,g)
        add_loose_reac(compt, c, prds[0], 'G', args_.rate_lose )
        txt_.append( '%s -> %s' % (c, prds[0]) )
        add_loose_reac(compt, c, prds[1], 'R', args_.rate_lose )
        txt_.append( '%s -> %s' % (c, prds[1]) )
    elif r > 0:
        #  print( '\tLoose only R', end = '' )
        prd =  'R%dG%d' % (r-1,g)
        txt_.append( '%s -> %s' % (c, prd))
        add_loose_reac(compt, c, prd, 'R', args_.rate_lose )
    elif g > 0:
        #  print( '\tLoose only B', end = '' )
        prd = 'R%dG%d' % (r,g-1)
        txt_.append( '%s -> %s' % (c, prd))
        add_loose_reac(compt, c, prd, 'G', args_.rate_lose )
    else:
        raise RuntimeError( 'Should not be here' )

def gain_subunit( compt, c ):
    global txt_
    #  print( "[INFO ] %s will gain a subunit" % c)
    r, g = int(c[1]), int(c[3])
    prds = 'R%dG%d' % (r+1,g), 'R%dG%d' % (r,g+1)
    add_gain_reac( compt, c, prds[0], 'R', args_.rate_gain )
    add_gain_reac( compt, c, prds[1], 'G', args_.rate_gain )
    txt_.append( '%s -> %s' % (c, prds[0]) )
    txt_.append( '%s -> %s' % (c, prds[1]) )
        
def add_solver( compt ):
    s = moose.Stoich( '%s/stoich' % compt.path )
    k = moose.Ksolve( '%s/ksolve' % compt.path )
    s.ksolve = k
    s.compartment = compt
    s.path = '%s/##' % compt.path
    
def save_data( plot = False ):
    import moose.utils as mu
    global tables_
    colocalized = []
    allCaMKII = []
    subunits = []
    for k in tables_:
        if k not in [ 'R', 'G' ]:
            allCaMKII.append( tables_[k].vector )
            r, g = int(k[1]), int(k[3])
            if r >= 1 and g >= 1:
                colocalized.append( tables_[k].vector )
        else:
            subunits.append( tables_[k].vector )

    t1 = moose.element( '/clock' ).currentTime 
    tvec = np.linspace( 0, t1, len(subunits[0])) / 60.0
    colocConc = np.sum(colocalized, axis=0)
    sumCaMKII = np.sum( allCaMKII, axis=0)
    coloc = 100*np.sum(colocalized, axis=0)/np.sum(allCaMKII, axis=0)

    outfile = args_.outfile or '%s.csv' % helper.dict2str(vars(args_))
    # data file
    df = pd.DataFrame()
    df['time'] = tvec
    df['colocalization_perc'] = coloc
    df['colocalization'] = colocConc
    df['all_camkii'] = sumCaMKII
    df.to_csv( outfile, index = False )
    print( "[INFO ] Data is saved to %s" % outfile )

    if args_.plot:
        #  plt.figure( figsize=(6,6) )
        ax1 = plt.subplot( 221 )
        plt.plot( tvec, colocConc, label = 'Mixed Color' )
        plt.plot( tvec, sumCaMKII, label = 'All CaMKII' )
        plt.xlabel( 'Time (min)' )
        plt.ylabel( 'Conc' )
        plt.legend( framealpha = 0, loc='best' )

        plt.subplot( 222, sharex = ax1)
        ps = fit_chain_reac(tvec, coloc )
        plt.plot( tvec, coloc, label = 'Colocalization' )
        plt.plot( tvec, double_exp(tvec, *ps), label='fit %s' % str(ps))
        plt.ylim( 0, 100 )
        plt.legend( framealpha = 0, loc='best' )
        plt.xlabel( 'Time (min)' )
        plt.ylabel( '% colocalization' )

        plt.subplot( 223, sharex = ax1)
        plt.plot( tvec, np.sum(subunits, axis=0), label = 'subunit' )
        plt.xlabel( 'Time (min)' )
        plt.legend( framealpha = 0, loc='best' )
        plt.suptitle( helper.dict2str(vars(args_)) )
        plt.tight_layout( rect = (0,0,1,0.95) )

        # write the network.
        g = nx.DiGraph()
        for r in moose.wildcardFind( '/##[TYPE=ZombieReac]' ):
            subs = [x.name for x in r.neighbors['sub']]
            prds = [x.name for x in r.neighbors['prd']]
            for s in subs:
                for p in prds:
                    if s in ['R','G'] or p in ['R','G']:
                        continue
                    g.add_edge(s, p)
        plt.subplot(224)
        plt.axis('off')
        nx.draw_networkx(g, font_size=4 )

        plt.savefig( '%s.png' % outfile )
        print( "[INFO ] Saved to file %s.png" %  outfile )


def build_model( compt ):
    global species_
    camkii7 = [ 'R%dG%d' % (i, 7-i) for i in range(8) ]
    camkii6 = [ 'R%dG%d' % (i, 6-i) for i in range(7) ]
    subBlue = 'G'
    subRed = 'R'
    for name in camkii6 + camkii7 + [ subRed, subBlue ]:
        add_species( compt, name )
    
    c0 = args_.camkii
    species_['R6G0'].concInit = c0 / 4.0
    species_['R7G0'].concInit = c0 / 4.0
    species_['R0G6'].concInit = c0 / 4.0
    species_['R0G7'].concInit = c0 / 4.0

    # setup reactions.
    # CaMKII7 can loose subunits.
    for c in camkii7:
        loose_subunit( compt, c )

    for c in camkii6:
        gain_subunit( compt, c )

    add_solver( compt )
    moose.reinit()
    with open( 'network.dot', 'w' ) as f:
        f.write( 'digraph model {\n' )
        f.write( '\t' + '\n\t'.join(txt_) )
        f.write( '\n}\n')

    moose.start( 3*60*60 )
    save_data()

def main( args ):
    global args_
    args_ = args
    moose.Neutral( '/model' )
    compt = moose.CubeMesh( '/model/cyl' )
    build_model( compt )

if __name__ == '__main__':
    import argparse
    # Argument parser.
    description = '''Colocalization experiment.'''
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--camkii', '-c'
        , required = False, type=float, default = 8e-3
        , help = '[CaMKII] in milli-molar concentration'
        )

    parser.add_argument('--rate-gain', '-rg'
        , required = False, type=float, default = 0.2
        , help = 'Rate of subunit picking /conc/sec'
        )

    parser.add_argument('--rate-lose', '-rl'
        , required = False, type=float, default = 0.5
        , help = 'Rate of subunit losing /sec'
        )
    parser.add_argument('--outfile', '-o'
        , required = False, type=str, default = ''
        , help = 'Save data to this file.'
        )
    parser.add_argument('--plot', '-p'
        , required = False, action = 'store_true'
        , default = False
        , help = 'Plot data ?'
        )

    class Args: pass 
    args = Args()
    parser.parse_args(namespace=args)
    main( args )

