#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function, division

__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import numpy as np
import pandas as pd
import itertools
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize as sco
from helper import *
try:
    import ajgar   # pip install git+https://github.com/dilawar/ajgar
except ImportError as e:
    subprocess.call([sys.executable, "-m", "pip", "install", 'ajgar', '--user'])

def charging_curve(t, tau):
    return 100*(1-np.exp(-t/tau))

def find_tau( t, y ):
    popt, pcorr = sco.curve_fit( charging_curve, t, y )
    return popt[0]

def plot_imshow( G, L, Z, ax ):
    Guniq = np.unique(G)
    Luniq = np.unique(L)
    r, c = len(Guniq), len(Z)//len(Guniq)
    img = np.reshape( Z[:r*c], (r, c) )
    im = ax.imshow( img, aspect = 'auto', interpolation='bicubic' )
    ax.set_title( r'half-life (min)' )

    # xlabel
    ax.set_xlabel( 'rate lose' )
    xN = max(1, len(Luniq)//5)
    yN = max(1, len(Guniq)//5)
    ax.set_xticks(range(0, len(Luniq), xN))
    ax.set_xticklabels( map(lambda x: '%.3f'%x, Luniq[::xN]) )

    # ylabel
    ax.set_ylabel( 'rate gain' )
    ax.set_yticks(range(0, len(Guniq), yN))
    ax.set_yticklabels(map(str,Guniq[::yN]))

    plt.colorbar(im, ax = ax)

def process( data ):
    # Groupby CaMKIi concentration.
    groups = itertools.groupby( data, lambda x: x[0].split('_')[0] )
    plt.figure( figsize=(10,10) )
    maxNumRows = 4
    for i, (gname, vals) in enumerate(groups):
        G, L, Z, ZZ = [], [], [], []
        ax = plt.subplot(maxNumRows, 3, 3*i+1)
        print( "[INFO ] Plotting for %s" % gname )
        for f, df in vals:
            ps = extract_params(f)
            label = 'kg=%.2f,kl=%.2f' % (ps[1][1], ps[2][1])
            T, C = df['time'], df['colocalization_perc']
            ax.plot( T, C, label = label)
            r = estimate_half_life( df['time'], df['colocalization_perc'] )
            try:
                tau = find_tau( df['time'].values, df['colocalization_perc'].values )
            except Exception as e:
                print( 'X', end = '' )
                continue
            #  if np.isnan(r):
                #  continue
            G.append(ps[1][1])
            L.append(ps[2][1])
            Z.append(r)
            ZZ.append(tau)
        ax.set_title( '[CaMKII]=%s mM' % gname[2:])
        ax.set_xlabel( 'Time (min)' )
        ax.set_ylim( 0, 100 )

        # plot phase plot.
        res = pd.DataFrame()
        res['rgain'] = G
        res['rlose'] = L
        res['half_life'] = Z 
        res['tau'] = ZZ
        res = res.sort_values( by = [ 'rgain', 'rlose' ] )
        res.to_csv( 'summary_%s.csv' % gname, index = False )

        import scipy.interpolate
        ax = plt.subplot( maxNumRows, 3, 3*i+2)
        cmap = mpl.cm.get_cmap()
        cmap.set_bad( color='white' )

        res = res.dropna()
        im = ax.tricontourf(res['rlose'], res['rgain'], res['half_life'], 20, cmap = cmap)
        ax.set_xscale( 'log' )
        ax.set_yscale( 'log' )
        plt.colorbar(im, ax = ax )
        ax.set_xlabel( 'rate lose' )
        ax.set_ylabel( 'rate gain' )
        ax.set_title( 'half-life' )

        ax1 = plt.subplot( maxNumRows, 3, 3*i+3)
        im = ax1.tricontourf(res['rlose'], res['rgain'], res['tau'], 20, cmap = cmap)
        ax1.set_xscale( 'log' )
        ax1.set_yscale( 'log' )
        plt.colorbar(im, ax = ax1 )
        ax1.set_xlabel( 'rate lose' )
        ax1.set_ylabel( 'rate gain' )
        ax1.set_title( 'tau' )

    plt.tight_layout()
    plt.savefig( 'summary.png' )

def main():
    import glob
    files = sorted(glob.glob('CM*.csv')
            , key=lambda x: float(x.split('_')[0][2:])
            )
    print( "[INFO ] Total %d files" % len(files) )
    process( [(f,pd.read_csv(f)) for f in files] )

if __name__ == '__main__':
    main()

