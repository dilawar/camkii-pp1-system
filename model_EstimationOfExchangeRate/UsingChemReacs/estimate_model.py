#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""estimate_model.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
import fnmatch
import helper
import itertools

def unique_params( params ):
    ps, fs = zip(*params)
    cms, rgs, rls = zip(*ps)
    return [ np.unique( [ x[1] for x in y ]) for y in zip(*ps) ]

def estimate_half_life_vs_rl( params, ax ):
    print( "[INFO ] Estimative half-life vs rate-lose" )
    # First we estimate for a fixed CaMKII concentration and fixed rg.
    cms, rgs, rls = unique_params(params)
    for cm, rg in itertools.product(cms, rgs):
        X, Y = [], []
        for r in rls:
            #  print( f"[INFO ] Estimating for rl={r}", end = ' ' )
            globPat = "CM%s*rg%s*rl%s*.csv" % (cm, rg, r)
            fs = sorted(glob.glob( globPat ))
            if len(fs) < 1:
                continue
            if not len(fs) == 1:
                print( 'X', end = '' )
                continue
            f = fs[0]
            df = pd.read_csv( f )
            hlife = helper.estimate_half_life(df['time'], df['colocalization_perc'])
            X.append( r )
            Y.append( hlife )
        ax.semilogx(X, Y, label=globPat)
        ax.set_xlabel( 'rate lose' )
        ax.set_ylabel( 'half life' )

def estimate_half_life_vs_rg( params, ax ):
    print( "[INFO ] Estimative half-life vs rate-lose" )
    # First we estimate for a fixed CaMKII concentration and fixed rg.
    cms, rgs, rls = unique_params(params)
    for cm, rl in itertools.product(cms, rls):
        X, Y = [], []
        for rg in sorted([float(x) for x in rgs]):
            #  print( f"[INFO ] Estimating for rl={r}", end = ' ' )
            globPat = "CM%s*rg%s*rl%s*.csv" % (cm, rg, rl)
            fs = sorted(glob.glob( globPat ))
            if len(fs) != 1:
                continue
            f = fs[0]
            df = pd.read_csv( f )
            hlife = helper.estimate_half_life(df['time'], df['colocalization_perc'])
            X.append( float(rg) )
            Y.append( hlife )
        ax.plot(X, Y, label=globPat)
        ax.set_xlabel( 'rate gain' )
        ax.set_ylabel( 'half life' )

def estimate_half_life_vs_cm( params, ax ):
    print( "[INFO ] Estimative half-life vs CM" )
    # First we estimate for a fixed CaMKII concentration and fixed rg.
    cms, rgs, rls = unique_params(params)
    for rg, rl in itertools.product(rgs, rls):
        X, Y = [], []
        for cm in sorted(cms, key=lambda x: float(x)):
            globPat = "CM%s*rg%s*rl%s*.csv" % (cm, rg, rl)
            fs = sorted(glob.glob( globPat ))
            if len(fs) != 1:
                continue
            f = fs[0]
            df = pd.read_csv( f )
            hlife = helper.estimate_half_life(df['time'], df['colocalization_perc'])
            X.append( float(cm) )
            Y.append( hlife )
        ax.semilogx(X, Y, label=globPat)
        ax.set_xlabel( '[CaMKII]' )
        ax.set_ylabel( 'half life' )

def main():
    files = glob.glob( 'CM*.csv' )
    print( "[INFO ] Total %d files" % len(files) )
    params = sorted([(helper.extract_params_str(f),f) for f in files ])

    ax1 = plt.subplot(221)
    ax2 = plt.subplot(222)
    ax3 = plt.subplot(223)
    
    estimate_half_life_vs_rl( params, ax1 )
    estimate_half_life_vs_rg( params, ax2 )
    estimate_half_life_vs_cm( params, ax3 )

    plt.tight_layout()
    plt.savefig( 'model_estimation.png' )

if __name__ == '__main__':
    main()

