import numpy as np
import scipy.optimize as sco

def dict2str( d, space = False ):
    m = str(d).replace( "'", '' )[1:-1]
    if not space:
        m = m.replace( ' ', '' )
    return m

def extract_params( f ):
    f = f.replace( '.csv', '' )
    fs = f.split( '_' )
    return [ (x[:2], float(x[2:])) for x in fs]

def extract_params_str( f ):
    f = f.replace( '.csv', '' )
    fs = f.split( '_' )
    return [ (x[:2], x[2:]) for x in fs]

def charging_curve_fit( t, tau ):
    return 100 * (1-np.exp(-t/tau))

def estimate_rate( xvec, yvec ):
    popt, pcorr = sco.curve_fit( charging_curve_fit, xvec, yvec )
    return popt[0]

def estimate_half_life( tvec, yvec ):
    for t, y in zip(tvec, yvec):
        if y > 50.0:
            return t
    return np.nan
