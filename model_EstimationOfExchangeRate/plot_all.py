#!/usr/bin/env python3
"""plot.py: 

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import re
import numpy as np
import pandas as pd

def main( files ):
    nrows = len(files)//2 + 1
    ax1 = plt.subplot( 221 )
    vals = [ ]
    for i, f in enumerate(sorted(files)):
        df = pd.read_csv( f, sep = ' ' )
        ax1.plot( df['Time'] , df['Val'], label = f )
        fname = os.path.basename( f )
        l = fname.replace( '.csv', '' ).split( '_' )
        l = [ int(re.search( '\d+', x).group(0)) for x in l ]
        plt.xlabel( 'Time (au)' )
        plt.ylabel( 'Co-localization' )
        vals.append( (l, df['Time'].values[-1] ) )

    vals = sorted( vals )

    # CaMKII, rg, rl
    # We fix the rate of loosing and gaining.
    ax2 = plt.subplot( 222 )
    ys = [ (x[0][0], x[1]) for x in vals if x[0][1]==10 and x[0][2]==10]
    X, Y = zip(*ys)
    ax2.plot( X, Y, '-o' )
    ax2.set_xlabel( '# CaMKII' )
    ax2.set_ylabel( 'Time to 60%' )
    ax2.set_title( 'rl=10, rg=10' )

    # We fix #CaMKII and rate of loosing
    ax2 = plt.subplot( 223 )
    ys = [ (x[0][1], x[1]) for x in vals if x[0][0]==100 and x[0][2]==10]
    X, Y = zip(*ys)
    ax2.plot( X, Y, '-o' )
    ax2.set_xlabel( 'rg' )
    ax2.set_ylabel( 'Time to 60%' )
    ax2.set_title( 'CaMKII=100, rl=10' )

    # We fix #CaMKII and rate of loosing
    ax2 = plt.subplot( 224 )
    ys = [ (x[0][2], x[1]) for x in vals if x[0][0]==100 and x[0][1]==10]
    X, Y = zip(*ys)
    ax2.plot( X, Y, '-o' )
    ax2.set_xlabel( 'rl' )
    ax2.set_ylabel( 'Time to 60%' )
    ax2.set_title( 'CaMKII=100, rg=10' )

    plt.tight_layout()
    plt.savefig( '%s.png' % __file__ )

if __name__ == '__main__':
    import glob
    main( glob.glob( '_data/*.csv' ) )
