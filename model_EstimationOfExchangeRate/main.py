#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
from __future__ import print_function, division

"""main.py: 

Estimate exchange rate.
Run with pypy for better performance.
"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import random
import math
import statistics
from collections import Counter

# 1 px = 10 nm. Not very precise but will do.
args_       = None
color1_     = (255,0,0)
color2_     = (0,255,0)
fra7to6     = 0.5               # Fraction of CaMKII7 to CaMKII6
camkiis_    = []
subunits_   = []

# Other global variables.
t_, tvec_   = 0.0, [0.0]

# colocalization.
res_        = [0.0] * 20

# rate of losing and rate of gaining.
def prob_of_losing():
    global args_
    return args_.rate_of_losing * args_.dt

def prob_of_gaining():
    global args_
    return args_.rate_of_gaining * args_.dt


def float_to_int( v ):
    vint = int(v)
    extra = v - int(v)
    if random.random() < extra:
        return vint + 1
    return vint

class Subunit():
    def __init__(self, color, x = None, y = None ):
        if x is None:
            x = int(random.uniform(0, N_))
        if y is None:
            y = int(random.uniform(0, N_))
        self.x, self.y = x, y
        self.color = color
        self.belogsTo = None

    def __repr__(self):
        return '%d, %d, color=%s' % (self.x, self.y, self.color)

class CaMKII( ):

    def __init__(self, symm ):
        global args_
        self.symm = symm
        self.x = int(random.uniform(0, args_.size))
        self.y = int(random.uniform(0, args_.size))

        # assign color to each of its subunit.
        self.color = color1_ if random.random() < 0.5 else color2_
        self.subunits = [Subunit(self.color, self.x, self.y) for i in range(self.symm)]

    def remove_subunit( self ):
        su = random.choice( self.subunits )
        self.subunits.remove( su )
        assert self.symm == 7, "Only CaMKII7 can loose subunit. %s" % self
        self.symm = 6
        return su

    def pickup_subunit( self, su ):
        assert self.symm == 6, "Only CaMKII6 can pickup a subunit. %s" % self
        self.symm = 7
        self.subunits.append( su )
        # The color of the holoenzyme is the color of majority.
        self.color = Counter( [x.color for x in self.subunits]).most_common(1)[0][1]

    def __repr__(self):
        return '%s: (%d, %d), %d' % (self.color, self.x, self.y, self.symm)


def show_frame(fI, save = False):
    import cv2
    global t_
    global res_
    img = draw()
    r = res_[-1]
    #  text = f'Time:  {t_:.3} s. Loc {r:.3}'
    text = 'Time: %s.f s. Loc %.3f' % (t_, r)
    cv2.putText(img, text, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255)
            , lineType=cv2.LINE_AA) 
    cv2.imshow( '2d', img )
    cv2.waitKey( 1 )
    if save:
        filename = os.path.join( '_figures', '%05d.png' % fI )
        cv2.imwrite( filename, img )

def draw( ):
    import cv2
    import numpy as np
    global camkiis_
    img = np.zeros(shape=(args_.size, args_.size, 3), dtype=np.uint8 )
    for c in camkiis_:
        r, delta = 4, 2*math.pi/len(c.subunits)
        for i, s in enumerate(c.subunits):
            dx, dy = int(r*math.cos(i*delta)), int(r*math.sin(i*delta))
            cv2.circle( img, (c.x+dx, c.y+dy), 1, s.color, -1)

    for s in subunits_:
        cv2.circle( img, (s.x, s.y), 1, s.color, -1)
    return img

def diffuse():
    global subunits_
    global args_
    diffConstInPx = args_.diff_const * (1e-6/(args_.density*1e-9))**2    # um^2/sec → px^2/sec
    d_ = (diffConstInPx * args_.dt)**0.5**0.5**0.5**0.5

    # CaMKII does not diffuse.
    for c in subunits_:
        if random.random() < 0.5:
            # step in x direction.
            dx = -float_to_int(d_) if random.random() < 0.5 else float_to_int(d_)
            c.x += dx 
            c.x = c.x % args_.size
        else:
            # step if y direction.
            dy = -float_to_int(d_) if random.random() < 0.5 else float_to_int(d_)
            c.y += dy
            c.y= c.y % args_.size

def looseOrGain():
    global camkiis_
    global subunits_ 

    # Foreach camkii.
    for cam in camkiis_:
        if cam.symm == 7:
            if random.random() < prob_of_losing():
                s = cam.remove_subunit( )
                # its safe to remove here.
                subunits_.append(s)
                continue

        elif 6 == cam.symm:
            # check if there is any subunit in the neighbourhood which can be
            # picked.
            for i, s in enumerate(subunits_):
                d = ((s.x-cam.x)**2 + (s.y-cam.y)**2)**0.5
                if d < 6.0:
                    # within the grabbing distance.
                    if random.random() < prob_of_gaining():
                        cam.pickup_subunit(s)
                        del subunits_[i]
                        break

def compute_result():
    global camkiis_
    global res_
    r = 0.0
    for c in camkiis_:
        cStats = Counter([s.color for s in c.subunits]).most_common(1)[0][1]
        if cStats < c.symm - 1:
            r += 1 / len(camkiis_)
    res_.append( r )
    res_.pop(0)

def simulate():
    global args_
    global camkiis_
    global t_
    for i in range( args_.camkii ):
        symm = 6 if random.random() < fra7to6 else 7
        camkiis_.append(CaMKII(symm))

    with open( args_.outfile, 'w' ) as f:
        f.write( 'Time Val\n' )

    i = 0
    while statistics.mean(res_[-20:]) < 0.6 and i < 1e7:
        i += 1
        # run for 30 mins.
        t_ += args_.dt
        diffuse()
        looseOrGain()
        compute_result()
        tvec_.append( t_ )

        if i % 200 == 0:
            # write data every 0.1 sec.
            #  print( f"[INFO ] T= {t_:.3}, Val=%.3f. Mean=%.3f" % (res_[-1], statistics.mean(res_)))
            print( "[INFO ] T= %.3f, Val=%.3f. Mean=%.3f" % (t_, res_[-1], statistics.mean(res_)))
            with open( args_.outfile, 'a' ) as f:
                f.write( '%g %g\n' % (t_, res_[-1]))
            if args_.visualize:
                show_frame( i, True )
    print( "[INFO ] Wrote to file %s" % args_.outfile )
    
def main( args ):
    global args_
    args_ = args
    print( "[INFO ] %s" % args_ )
    try:
        simulate()
    except KeyboardInterrupt as e:
        print( 'Got Ctrl+C' )

if __name__ == '__main__':
    import argparse
    # Argument parser.
    description = '''Estimate rate of subunit exchange.'''
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--camkii', '-c'
        , required = False, default = 100, type=int
        , help = 'No of CaMKII to release in squre arena.'
        )
    parser.add_argument('--size', '-s', default = 1000
        , help = 'Size of the square arena'
        )
    parser.add_argument('--density', '-d'
        , required = False, default = 10
        , help = 'x where 1px=x nm.'
        )
    parser.add_argument( '--diff-const', '-D'
        , required = False, default = 1.0
        , help = 'Diffusion coefficient of subunits (um^2/sec)'
        )
    parser.add_argument('--rate-of-losing', '-rl'
        , required = False, default = 10.0, type = float
        , help = 'Rate of losing subunit (per sec)'
        )
    parser.add_argument('--rate-of-gaining', '-rg'
        , required = False, default = 10.0, type = float
        , help = 'Rate of gaining subunit (per sec)'
        )
    parser.add_argument('--dt', '-dt'
        , required = False, default = 1e-3
        , help = 'Timestep (sec)'
        )
    parser.add_argument('--outfile', '-o'
        , required = False , default = '%s.csv' % __file__
        , help = 'Write data to this file.'
        )
    parser.add_argument('--visualize', '-v'
        , action = 'store_true'
        , help = 'Visualize simulation'
        )
    class Args: pass 
    args = Args()
    parser.parse_args(namespace=args)
    main( args )
