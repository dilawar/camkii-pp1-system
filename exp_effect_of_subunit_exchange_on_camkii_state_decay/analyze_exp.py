#!/usr/bin/env python
"""
Ananlyze this experiment.

The system is turned ON by a strong calcium pusle. This experiment records the
decay dyamics of CaMKII.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Dilawar Singh"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import math
import os
from collections import defaultdict, OrderedDict
import matplotlib as mpl
import matplotlib.colors as colors
mpl.use( 'Agg' )
import pandas as pd
import matplotlib.pyplot as plt

plt.rc('axes', linewidth = 0.1 )
plt.rc('axes', labelsize = 'small' )
plt.rc('font', family='serif')
plt.rc('text', usetex = True )

import numpy as np
import pandas as p
import subprocess
import glob
import re

args_ = None
dt_ = 3.0

def intToColor( val, max_levels = 10 ):
    cmap = mpl.cm.get_cmap('Spectral', max_levels)
    return cmap( val /float(max_levels))

def decay_time( tvec, yvec ):
    thresH = 0 # yvec.max( ) / 4.0
    startT, endT = 0, 0
    intervalStarted = False
    slices = [ ]
    for i, v in enumerate( yvec ):
        if not intervalStarted and v > thresH:
            startT = tvec[ i ]
            intervalStarted = True
        if intervalStarted and v <= thresH:
            intervalStarted = False
            endT = tvec[i]
            slices.append( ( startT, endT ) )

    activity = [ ]
    for t1, t2 in slices:
        activity.append( t2 - t1 )
    
    # This means the system was always ON.
    if not slices:
        activity.append( tvec.max( ) )

    # Flush out odd number. The very small numbers are usually stupid.
    maxAct = max( activity )
    activity = list( filter( lambda x : x > maxAct / 2.0, activity ))
    m, s = np.mean( activity ), np.std( activity )
    return m, s

def normalized_activity( tvec, yvec ):
    """
    Area under the curve divided by max time 
    """
    yvec = yvec / yvec.max( )
    tdiff = np.diff( tvec )
    return np.sum( tdiff * yvec[:-1] ) / tvec.max( )

def plotImshow( ax, data ):
    if not data:
        print( '[WARNING] Empty data' )
        return 
    ratio, activity, camkiis, pp1s = zip( *data )
    img = np.zeros( shape=(max(pp1s)+1, max(camkiis)+1 ) )
    for r, a, c, p in data:
        img[p,c] = a

    cf = ax.imshow( img, interpolation = 'none', aspect = 'auto' )
    plt.colorbar( cf, ax = ax )

def process_file( filewithregex, plot = True ):
    global args_
    m, f = filewithregex
    camkii = int( m.group( 1 ) )
    pp1 = int( m.group(2) )
    suEnabled = m.group( 3 )

    print( 'Reading %s' % f )
    d = p.read_csv( f, comment='#', sep = ' ', low_memory = False )

    tvec = d[ 'time' ]
    camkiiVec = d[ 'CaMKII*']
    pp1Vec = d[ 'PP1' ]

    return d, active_camkii_slices( tvec, camkiiVec )


def active_camkii_slices( t, camkii ):
    global dt_
    global args_
    activeSlices = [ ]
    maxT = t.max( )
    xs = np.arange( 0, int(t.max( ) / dt_), int(args_.timeperiod / dt_) )
    for x1, x2 in zip( xs, xs[1:] ):
        activeSlices.append( camkii[x1:x2-3].values )
    return activeSlices[1:]


def search_files( ):
    global args_
    files = [ ]
    pat = r'CaM%d.*PP%d.*_processed.dat' % (args_.camkii, args_.pp1 )
    for d, sd, fs in os.walk( '.' ):
        for f in fs:
            if re.search( pat, f ):
                files.append( os.path.join(d, f ) )

    if len( files ) < 1:
        print( '[WARN] No files found for pat %s' % pat )
        quit( )
    return files


def process( file_dict, usecache = True ):
    nrows = len( file_dict )
    files = file_dict.keys( )
    cacheFile = '__data.pickle' 
    if os.path.isfile( cacheFile ):
        print( '[WARN] using cached data from %s' % cacheFile ) 
        df = pd.read_pickle( cacheFile )
    else:
        usecache = False

    if not usecache:
        df = pd.DataFrame( )
        for i, (camkii, pp1) in enumerate( sorted( files ) ):
            title = 'CaM=%d,PP1=%d' % (camkii, pp1)
            print( "\nAnalysing for CaMKII=%s and PP=%s" % (camkii, pp1) )
            files = file_dict[ (camkii, pp1) ]
            for matchFile in files:
                series = pd.Series( )
                su = matchFile[0].group(3)
                series[ 'subunit_exchange' ] = su
                d, activeSlices = process_file( matchFile )
                series[ 'active_slices' ] = activeSlices
                series[ 'camkii'] = camkii
                series[ 'pp1' ] = pp1
                df = df.append( series, ignore_index = True )
        df.to_pickle( cacheFile )

    # Symmary figure.
    camkiis = np.unique( df[ 'camkii' ] )
    nrows = len( camkiis )


    plt.figure( figsize=(8,4) )
    ax1 = plt.subplot( 121 )
    ax2 = plt.subplot( 122 )

    res = pd.DataFrame( )
    for i, camkii in enumerate( camkiis ):
        for ii, su in enumerate( [ 'ON', 'OFF' ] ):
            d1 = df[ df[ 'subunit_exchange' ] == su ]
            data = d1[ d1['camkii'] == camkii ] 

            # Append zeros to array to make them all same size.
            slices = [ ]
            normedSlices = [ ]
            for ss in data[ 'active_slices']:
                for s in ss:
                    try:
                        slices.append( 1.0 * s / camkii )
                    except Exception as e:
                        print( 'Failed to normalize slice %s' % s )

            print( "[INFO ] Total slices %d" % len(slices) )
            assert len(slices) > 0

            u, sigma = np.mean( slices, axis=0), np.std( slices, axis = 0 )
            xvec = np.arange( 0, len(u) ) * dt_
            ax1.plot( xvec, u, lw = 2
                    , color = intToColor( ii*50, 51 )
                    , label = 'SU=%s' % su 
                    )


            ax1.fill_between( xvec
                    , u - sigma, u + sigma, alpha = 0.4
                    , color = intToColor( ii*50, 51 )
                    , lw = 0
                    )
            ax1.set_xlabel( 'Time (sec)' )
            ax1.set_ylabel( 'Fraction of active CaMKII' )
            ax1.legend( )

            actS = np.hstack( slices )
            hist, bins = np.histogram( actS, bins = 20 )
            ax2.plot( hist , lw = 2, label = 'SU=%s' % su)
            ax2.legend(loc='best', framealpha=0.4, fontsize = 8)
            ax2.set_xlabel( 'Active CaMKII' )
            
            # Save them in dataframe.
            res[ 'time' ] = xvec
            res[ 'decay_su%s_mean' % su ] = u
            res[ 'decay_su%s_std' % su ] = sigma

    plt.tight_layout( pad = 0.4, rect = (0,0,1,0.9) )
    outfile = 'summary.png'
    plt.savefig( outfile )
    print( 'Saved to %s' % outfile )

    res.to_csv( 'data_camkii_state_decay.csv', index = False )
    print( '[INFO] Saved data to data_camkii_state_decay.csv' )


def main( args ):
    global args_, dt_
    args_ = args
    dt_ = args.dt
    files = search_files( ) 
    fileDict = defaultdict( list )
    for f in files:
        m = re.search( r'CaM(\d+).+?PP(\d+).+su((ON|OFF))', f )
        if m:
            camkii, pp1 = int(m.group(1)), int(m.group(2))
            fileDict[ (camkii, pp1) ].append( (m,f) )

    process( fileDict, usecache = args_.no_cache )

if __name__ == '__main__':
    import argparse
    # Argument parser.
    description = '''Analyze data.'''
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--camkii', '-c'
        , required = True, type = int
        , help = 'CaMKII numbers'
        )
    parser.add_argument('--pp1', '-p'
        , required = True, type = int
        , help = 'Number of PP1'
        )
    parser.add_argument( '--max', '-m'
        , required = False
        , default = -1, type = int
        , help = 'Maximum number of files to process'
        )
    parser.add_argument( '--no-cache', '-nc'
        , action = 'store_true'
        , help = 'Do not use cached data'
        )
    parser.add_argument('--timeperiod', '-t'
        , required = False, default = 0, type = int
        , help = 'Period of ca-input'
        )
    parser.add_argument('--dt'
        , required = False, type = float, default = 1.0
        , help = 'DT for recording.'
        )
    class Args: pass 
    args = Args()
    parser.parse_args(namespace=args)
    main( args )
