#!/usr/bin/env bash

# CaMKII and PP1 are distributed among NUMVOXELS. Make sure that NUMVOXELS are
# large enough and contain only 1 or 2 CaMKII.
RECORD_DT=5
DIFFDICT="{ 'x' : 0e-18, 'y' : 0e-18, 'PP1' : 1e-13 }"
SIMTIME=3
CAMKII=100
PP1=90000
NUMVOXELS=20
PERIOD=1000

outfile="CaM${CAMKII}_PP${PP1}_voxels${NUMVOXELS}_suOFF.dat"
python ../camkii_pp1_scheme.py --camkii $CAMKII --pp1 $PP1 \
    --simtime $SIMTIME \
    --record-dt $RECORD_DT \
    --ca-expr "(fmod(t,$PERIOD)<5)?5e-3:80e-6" \
    --num-voxels $NUMVOXELS \
    -o ${outfile}

python ./analyze.py -i ${outfile}

outfile="CaM${CAMKII}_PP${PP1}_voxels${NUMVOXELS}_suON.dat"
python ../camkii_pp1_scheme.py --camkii $CAMKII --pp1 $PP1 \
    --simtime $SIMTIME \
    --record-dt $RECORD_DT \
    --diff-dict "$DIFFDICT" \
    --ca-expr "(fmod(t,$PERIOD)<5)?5e-3:80e-6" \
    --num-voxels $NUMVOXELS --enable-subunit-exchange  \
    -o ${outfile}

python ./analyze.py -i ${outfile}

echo "Analysing the experiment"
python ./analyze_exp.py -c ${CAMKII} -p ${PP1} -t $PERIOD --dt $RECORD_DT
