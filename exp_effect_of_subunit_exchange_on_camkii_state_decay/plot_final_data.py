"""plot_final_data.py: Plots the final data generated post processing.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Dilawar Singh"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os

import matplotlib.pyplot as plt
plt.style.use( 'seaborn-talk' )
plt.rc( 'text', usetex = True )

import scipy.interpolate
import math

import numpy as np
import pandas as pd

pulsePeriod = 2 * 86400 
print( '[ALERT] Make sure to check what is the time when calcium pulse is '
        ' applied. I am using default value of 2 days i.e. ' + str( pulsePeriod) 
        + ' secs '
        )

def plot( infile ):
    df = pd.read_csv( infile, sep = ',' )
    withSu = df[ df[ 'su_enabled' ] == 1 ]
    withoutSu = df[ df[ 'su_enabled'] == 0 ]
    
    activityWithSu = withSu[ 'mean' ].values
    activityWithoutSu = withoutSu[ 'mean' ].values

    activityWithSu[ activityWithSu >= pulsePeriod ] = pulsePeriod
    activityWithoutSu[ activityWithoutSu >= pulsePeriod ] = pulsePeriod 

    ax = plt.subplot( 2, 1, 1 )
    ax.semilogy( withSu[ 'pp1' ] / withSu[ 'camkii']
            , activityWithSu, 'o', label = 'Subunit exchange'
            )
    ax.semilogy( withoutSu[ 'pp1' ] / withoutSu[ 'camkii']
            , activityWithoutSu , '.', label = 'No subunit exchange'
            )
    ax.legend( )
    ax.set_xlabel( r'$\frac{PP1}{CaMKII}$' )
    ax.set_ylabel( r'$log( \tau )$' )

    # Annotation
    ax.annotate( r'$Ca$ pulse timeperiod', (1, pulsePeriod ), (1.5, 2*pulsePeriod) 
                , arrowprops = { 'arrowstyle' : '->' }
            )


    activityData = { 'With SU' : withSu, 'Without SU' : withoutSu }
    for i, title in enumerate( activityData ):
        data = activityData[ title ]
        ax = plt.subplot( 2, 2, 3 + i )
        # Now the phase plot.
        camkii, pp1, activity = data['camkii'].values, \
                data['pp1'].values, data['mean'].values

        activity[ activity >= pulsePeriod ] = pulsePeriod

        m, n =  max(camkii)+1, max( pp1) + 1
        activityImg = np.zeros( shape=(m,n) )
        for k, (i, j) in enumerate( zip( camkii, pp1 ) ):
            activityImg[ i, j ] = math.log( activity[ k ] )

        dffi = scipy.interpolate.Rbf( 
                camkii, pp1, np.log10( activity )
                # , function = 'linear' 
                )

        img =  [ ]
        N = max( m, n )
        for i in range( m ):
            xi = [i] * N
            yi = range( N )
            acti = dffi( xi, yi )
            img.append( acti )

        img = np.matrix( img )
        print( img.max(), img.min() )
        plt.imshow( img, interpolation = 'none', aspect = 'auto', cmap='viridis' )
        plt.xlabel( r'$PP1$' )
        plt.ylabel( r'$CaMKII$' )
        plt.title( r'$log( \tau )$ %s' % title )
        plt.colorbar( )

    plt.suptitle( r'Decay time $(\tau)$' )
    plt.tight_layout( rect = (0,0, 1, 0.95) )
    plt.savefig( '%s.png' % infile )
    # plt.show( )

def main( ):
    infile = './_final_data.csv'
    plot( infile )

if __name__ == '__main__':
    main()
