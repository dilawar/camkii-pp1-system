#!/usr/bin/env python3

"""observability_camkii.py: 


"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import networkx as nx
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from graphviz import Source 

mpl.style.use( 'bmh' )
mpl.rcParams['axes.linewidth'] = 0.2
mpl.rcParams['lines.linewidth'] = 1.0
mpl.rcParams['text.usetex'] = False


def main( ):
    dotfile = './network_camkii_pp1.dot'
    g = nx.drawing.nx_agraph.read_dot( dotfile )

    # Find the SCC
    sccs = nx.strongly_connected_components( g )

    sccColors = { }
    minimal = [ ]
    for i, scc in enumerate(sccs):
        indeg = 0
        for n in scc:
            indeg += g.in_degree(n) 
            sccColors[ n ] = i + 1
        if indeg == 0:
            minimal.append( scc )

    colors = [ ]
    for n in g.nodes( ):
        colors.append( sccColors.get( n, 0 ) )


    pos = nx.drawing.nx_agraph.graphviz_layout( g, 'neato' )
    nx.draw_networkx( g, pos = pos, node_color = colors 
            , font_size = 6
            )

    for m in minimal:
        nx.draw_networkx_nodes( m, pos = pos
                , node_color = 'b'
                , node_size = 800
                , alpha = 0.5 )

    plt.title( 'Neccessary sensors' ) 
    plt.savefig( '%s.png' % sys.argv[0] )

if __name__ == '__main__':
    main()
