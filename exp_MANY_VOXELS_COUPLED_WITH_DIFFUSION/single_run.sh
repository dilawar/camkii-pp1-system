#!/usr/bin/env bash
Dpp=5e-13

for Dsub in 1e-13 3e-13 5e-13 1e-14 5e-14; do
    ( 
        OUTFILE="test-Dsub-${Dsub}+Dpp-${Dpp}.single.dat"
        python ../camkii_pp1_scheme.py \
            --camkii 6 --pp1 180 \
            --diff-dict "{'x':$Dsub,'y':$Dsub,'PP1':$Dpp}" \
            --num-voxels 3 \
            --voxel-length 180e-9 \
            --enable-subunit-exchange \
            --simtime 10 \
            -o $OUTFILE
        python ./analyze.py -i $OUTFILE -o ${OUTFILE}.png
    ) &
done
wait
