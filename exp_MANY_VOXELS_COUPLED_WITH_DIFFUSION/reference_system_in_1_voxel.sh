#!/usr/bin/env bash
python ../camkii_pp1_scheme.py \
    --camkii 6 --pp1 160 \
    --num-voxels 1 \
    --voxel-length 180e-9 \
    --enable-subunit-exchange \
    --simtime 20 \
    -o ref.dat
python ./analyze.py -i ref.dat -o ref.dat.png
