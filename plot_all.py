"""plot_all.py: 

Plot CaMKII/PP1 data.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2016, Dilawar Singh"
__credits__          = ["NCBS Bangalore"]
__license__          = "GNU GPL"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import matplotlib.pyplot as plt
import matplotlib
import subprocess
import datetime
import datetime
import numpy as np
import re
import pandas

matplotlib.style.use( 'seaborn-poster' )
matplotlib.rcParams['axes.linewidth'] = 0.1 #set the value globally

gitHash = subprocess.check_output(
        ["git", "log", "--pretty=format:'%h'", "-n", "1" ]
        )
now_ = datetime.datetime.today().ctime()
title_ = 'Git# %s, @ %s' % ( gitHash, now_ )

def sorteStates( state ):
    try:
        index = state.split( '.' )[1]
        x, y = int(index[1]), int(index[3])
        return (x+y,x)
    except Exception as e:
        return state

def process( data ):
    tVec = data['time'][1:]
    t = tVec / (24 * 3600.0 )   # time in days
    nPlots = 0
    plots = []
    togetherPlots = []
    for k in sorted(data.columns, key = sorteStates):
        if k == 'time':
            continue
        y = data[k][1:]   # drop the first line
        # if y.min() == y.max():
            # print( '[INFO] No change in %s : %f' % (k, y.min()) )
        if re.search( r'\.x\d+y\d+', k):
            plots.append((y,k))
        elif re.search( r'\.x\d+P', k):
            togetherPlots.append((y,k))
        elif 'PP1' in k:
            plots.append( (y,k))

    plt.figure( figsize=(10, 2*len(plots)) )
    totalPlots = len( plots ) + 1
    for i, (y, k) in enumerate(plots):
        plt.subplot( totalPlots, 1, i + 1 )
        plt.plot( t, y, label = k )
        plt.legend(loc='best', framealpha=0.1)

    plt.subplot( totalPlots, 1, totalPlots )
    window = np.ones( 200 ) / 200.0
    for (y,k) in togetherPlots:
        y = np.convolve( y, window, 'same' )
        plt.plot( t, y, label = k )
        plt.legend(loc='best', framealpha=0.1)


    plt.xlabel( 'Time in days' )
    plt.suptitle( title_ )
    plt.tight_layout( )
    plt.savefig( './all_states_camkii.png' )
    plt.close()
    # plt.show( )


def main():
    data = pandas.read_csv( "./weakly_interacting_psd.csv", delimiter = ' ' )
    process( data )

if __name__ == '__main__':
    main()

