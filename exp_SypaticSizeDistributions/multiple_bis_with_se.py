"""multiple_bis_with_se.py 

Group of multistables with subunit-exchange enabled.  Subunit exchage is
probably going to cause asymmetry.
"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import random
import matplotlib.pyplot as plt
import numpy as np
import math
from bistable import Bistable

def compute_effect( x, others ):
    # Small system do not have any impact me me
    toConsider = [ o.xs[-1] for o in others if o.x1 > x.x1 and o.xs[-1] > 1 ]
    return sum(toConsider) * 1e-1

def main():
    plt.figure()

    system = []
    dist = [4]*8 + [8]*4 + [12]*2 + [16]
    #  dist = random.choice( [2,3,4,5,6], weight=[10,8,6,4,2,1] )
    for d in dist:
        p = 100.0 / (2**d) 
        a = Bistable(p, p, d)
        system.append(a)

    N = 10000
    for i in range(N):
        for x in system:
            deltaEffect = compute_effect(x, system)
            x.step( pupDelta = deltaEffect )
    
    # plots.
    gridSize = (4, 2)
    plt.figure( figsize=(6, 6) )
    ax1 = plt.subplot2grid( gridSize, (0,0), colspan = 2, rowspan = 2 )
    ax4 = plt.subplot2grid( gridSize, (2,0), colspan = 1 )
    ax5 = plt.subplot2grid( gridSize, (2,1), colspan = 1 )
    allHistAx = plt.subplot2grid( gridSize, (3,1), colspan = 1 )

    allStates = []
    offset, toShow = 0, min(3000, N)
    for b in system:
        allStates.append(b.xs)
        y = np.array(b.xs)
        ax1.plot( y[:toShow] + offset, '-,', color='black', lw=0.2 )
        allHistAx.hist( y, range=(0,max(dist))
                #  , bins=max(dist)+1
                #  , histtype='step'
                , density = True 
                )
        offset += y.max() + 1
    ax1.set_xlabel( 'Step' )
    ax1.set_title( 'Independant bistables: %d steps are shown' % toShow )

    # sums
    #  allX = np.average( allStates, axis = 0, weights=[10,3,1] )
    allX = np.sum( allStates, axis = 0 )
    ax4.plot( allX )
    ax4.set_title( 'Sum of all activity' )

    ax5.hist( allX )
    ax5.set_title( 'Histogram' )

    plt.tight_layout( )
    plt.savefig( '%s.png' % __file__ )

if __name__ == '__main__':
    main()
