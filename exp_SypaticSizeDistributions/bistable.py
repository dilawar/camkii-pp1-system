"""bistable.py: 

A bistable switch.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import random

class Bistable():

    def __init__(self, pup, pdown, x1=1):
        global active_su_
        print( f"↕ pup={pup} pdown={pdown} x1={x1}" )
        self.pup   = pup
        self.pdown = pdown
        self.x1    = x1
        self.x0    = 0
        self.x     = 0 if random.random() < 0.5 else x1
        self.xs    = [ self.x ]
        self.err   = 0.2    # Fluctuations.

    def step( self, pupDelta = 0.0, pdownDelta = 0.0 ):
        if self.x < self.x1 * self.err:
            # We are in  low state
            if random.random() < (self.pup + pupDelta):
                # Go HIGH
                self.x = random.uniform(self.x1*(1-self.err), self.x1)
            else:
                self.x = random.uniform(0, self.err)
        elif self.x > self.x1*(1-self.err):
            if random.random() < (self.pdown + pdownDelta):
                # Go LOW
                self.x = random.uniform(0, self.err)
            else:
                self.x = random.uniform(self.x1*(1-self.err), self.x1)
        else:
            raise RuntimeError( 'Why are we here' )

        # add current state to trajectory
        self.xs.append( self.x )

    def steps( self, N = 100 ):
        [ self.step() for x in range(N) ]

