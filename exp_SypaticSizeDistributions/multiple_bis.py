"""multiple_bis.py: 

Group of multistables.

"""
    
__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2017-, Dilawar Singh"
__version__          = "1.0.0"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"
__status__           = "Development"

import sys
import os
import random
import matplotlib.pyplot as plt
import numpy as np
import math
from bistable import Bistable


def main():
    plt.figure()

    N = 3000
    system = []
    dist = [2]*4 + [4]*2 + [8]
    for d in dist:
        p = 1.0 / (2**d) 
        a = Bistable(p, p, d)
        a.steps(N)
        system.append(a)

    # plots.
    gridSize = (3, 2)
    ax1 = plt.subplot2grid( gridSize, (0,0), colspan = 2, rowspan = 2 )
    ax4 = plt.subplot2grid( gridSize, (2,0), colspan = 1 )
    ax5 = plt.subplot2grid( gridSize, (2,1), colspan = 1 )

    allStates = []
    offset = 0
    for b in system:
        allStates.append(b.xs)
        y = np.array(b.xs)
        ax1.plot( y + offset, '-,', color='black', lw=0.2 )
        offset += y.max() 

    ax1.set_xlabel( 'Step' )
    ax1.set_title( 'Independant bistables' )

    # sums
    #  allX = np.average( allStates, axis = 0, weights=[10,3,1] )
    allX = np.sum( allStates, axis = 0 )
    ax4.plot( allX )
    ax4.set_title( 'Sum of all activity' )

    ax5.hist( allX )
    ax5.set_title( 'Histogram' )

    plt.tight_layout( )
    plt.savefig( '%s.png' % __file__ )

if __name__ == '__main__':
    main()
