# Thursday 27 September 2018 11:53:50 AM IST

In [](./multiple_bis.py.png) we have multiple independent bistables together.
They do not give rise to log-normal distributions of synaptic size. As expected,
sum of them converges to a Gaussian.

We enable subunit exchange, i.e. when a cluster is active, it releases subunits
and they helps in activating other clusters. In fact, subunit exchange cause
multiple bistables to sync. Therefore subunit exchange can be approximated by a
large bistable switch. 
